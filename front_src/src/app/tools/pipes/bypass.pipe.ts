import { DomSanitizer } from '@angular/platform-browser'
import { Pipe, PipeTransform } from "@angular/core";

// USER-CODE-O
// USER-CODE-C

@Pipe({ name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@Pipe({name: 'safeStyle'})
export class SafeStylePipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer) {}

  transform(style) {
    return this.sanitized.bypassSecurityTrustStyle(style);
  }
}

// USER-CODE-O
// USER-CODE-C
