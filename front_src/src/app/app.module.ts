import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routes';
import { AppService } from './app.service';
import { AppSettings } from './app.settings';
import { PipesModule } from './tools/pipes.module';
import { HttpService } from './http.service';
import { NavigationComponent } from './components/navigation/navigation.component';
import { PreviewComponent } from './preview.component';
import { TranslationService } from './translation.service';

import { TermsofserviceComponent } from './containers/termsofservice/termsofservice.component';
import { PrivacypolicyComponent } from './containers/privacypolicy/privacypolicy.component';
import { GetkaitComponent } from './containers/getkait/getkait.component';
import { LoginComponent } from './containers/login/login.component';
import { ContactComponent } from './containers/contact/contact.component';
import { HomeComponent } from './containers/home/home.component';

import { BlingRecordFileItemComponent } from './components/bling/blingrecordfile-item/blingrecordfile-item.component';
import { BlingRecordFileListComponent } from './components/bling/blingrecordfile-list/blingrecordfile-list.component';
import { BlingFileItemComponent } from './components/bling/blingfile-item/blingfile-item.component';
import { BlingFileListComponent } from './components/bling/blingfile-list/blingfile-list.component';
import { BlingUserGroupItemComponent } from './components/bling/blingusergroup-item/blingusergroup-item.component';
import { BlingUserGroupListComponent } from './components/bling/blingusergroup-list/blingusergroup-list.component';
import { BlingLanguageItemComponent } from './components/bling/blinglanguage-item/blinglanguage-item.component';
import { BlingLanguageListComponent } from './components/bling/blinglanguage-list/blinglanguage-list.component';
import { BlingRouteItemComponent } from './components/bling/blingroute-item/blingroute-item.component';
import { BlingRouteListComponent } from './components/bling/blingroute-list/blingroute-list.component';
import { BlingTranslationItemComponent } from './components/bling/blingtranslation-item/blingtranslation-item.component';
import { BlingTranslationListComponent } from './components/bling/blingtranslation-list/blingtranslation-list.component';
import { BlingMetaSnippetItemComponent } from './components/bling/blingmetasnippet-item/blingmetasnippet-item.component';
import { BlingMetaSnippetListComponent } from './components/bling/blingmetasnippet-list/blingmetasnippet-list.component';
import { BlingMetaTagItemComponent } from './components/bling/blingmetatag-item/blingmetatag-item.component';
import { BlingMetaTagListComponent } from './components/bling/blingmetatag-list/blingmetatag-list.component';
import { BlingRouteTranslationItemComponent } from './components/bling/blingroutetranslation-item/blingroutetranslation-item.component';
import { BlingRouteTranslationListComponent } from './components/bling/blingroutetranslation-list/blingroutetranslation-list.component';
import { BlingTagItemComponent } from './components/bling/blingtag-item/blingtag-item.component';
import { BlingTagListComponent } from './components/bling/blingtag-list/blingtag-list.component';
import { BlingRecordTagItemComponent } from './components/bling/blingrecordtag-item/blingrecordtag-item.component';
import { BlingRecordTagListComponent } from './components/bling/blingrecordtag-list/blingrecordtag-list.component';
import { BlingTemplateItemComponent } from './components/bling/blingtemplate-item/blingtemplate-item.component';
import { BlingTemplateListComponent } from './components/bling/blingtemplate-list/blingtemplate-list.component';
import { BlingTemplateFileItemComponent } from './components/bling/blingtemplatefile-item/blingtemplatefile-item.component';
import { BlingTemplateFileListComponent } from './components/bling/blingtemplatefile-list/blingtemplatefile-list.component';
import { SocialMediaItemComponent } from './components/entities/socialmedia-item/socialmedia-item.component';
import { SocialMediaListComponent } from './components/entities/socialmedia-list/socialmedia-list.component';
import { TopElementsItemComponent } from './components/entities/topelements-item/topelements-item.component';
import { ReachClientsItemComponent } from './components/entities/reachclients-item/reachclients-item.component';
import { CustomerServiceItemComponent } from './components/entities/customerservice-item/customerservice-item.component';
import { BottomDescriptionItemComponent } from './components/entities/bottomdescription-item/bottomdescription-item.component';
import { CopyrightItemComponent } from './components/entities/copyright-item/copyright-item.component';
import { ServicePackagesItemComponent } from './components/entities/servicepackages-item/servicepackages-item.component';
import { BenefitsItemComponent } from './components/entities/benefits-item/benefits-item.component';
import { ContactFormSettingsItemComponent } from './components/entities/contactformsettings-item/contactformsettings-item.component';
import { LoginSectionItemComponent } from './components/entities/loginsection-item/loginsection-item.component';
import { PrivacyPolicyItemComponent } from './components/entities/privacypolicy-item/privacypolicy-item.component';
import { TermsOfServiceItemComponent } from './components/entities/termsofservice-item/termsofservice-item.component';


// USER-CODE-O
import { FooterComponent } from "./components/custom/footer/footer.component";
import { MenuComponent } from "./components/custom/menu/menu.component";
import { ContactFormComponent } from './components/custom/contact-form/contact-form.component';
import { ContactUsItemComponent } from "./components/entities/contactus-item/contactus-item.component";
import { GetKaitFormItemComponent } from "./components/entities/getkaitform-item/getkaitform-item.component";
import { AnimationsComponent } from './components/custom/animations/animations.component';
import { RedirectButtonComponent } from './components/custom/redirect-button/redirect-button.component';
// USER-CODE-C

@NgModule({
  declarations: [
    AppComponent,
    PreviewComponent,
    NavigationComponent,
    TermsofserviceComponent,
    PrivacypolicyComponent,
    GetkaitComponent,
    LoginComponent,
    ContactComponent,
    HomeComponent,

    BlingRecordFileItemComponent,
    BlingRecordFileListComponent,
    BlingFileItemComponent,
    BlingFileListComponent,
    BlingUserGroupItemComponent,
    BlingUserGroupListComponent,
    BlingLanguageItemComponent,
    BlingLanguageListComponent,
    BlingRouteItemComponent,
    BlingRouteListComponent,
    BlingTranslationItemComponent,
    BlingTranslationListComponent,
    BlingMetaSnippetItemComponent,
    BlingMetaSnippetListComponent,
    BlingMetaTagItemComponent,
    BlingMetaTagListComponent,
    BlingRouteTranslationItemComponent,
    BlingRouteTranslationListComponent,
    BlingTagItemComponent,
    BlingTagListComponent,
    BlingRecordTagItemComponent,
    BlingRecordTagListComponent,
    BlingTemplateItemComponent,
    BlingTemplateListComponent,
    BlingTemplateFileItemComponent,
    BlingTemplateFileListComponent,
    SocialMediaItemComponent,
    SocialMediaListComponent,
    TopElementsItemComponent,
    ReachClientsItemComponent,
    CustomerServiceItemComponent,
    BottomDescriptionItemComponent,
    CopyrightItemComponent,
    ServicePackagesItemComponent,
    BenefitsItemComponent,
    ContactFormSettingsItemComponent,
    LoginSectionItemComponent,
    PrivacyPolicyItemComponent,
    TermsOfServiceItemComponent,

    // USER-CODE-O
    FooterComponent,
    MenuComponent,
    ContactFormComponent,
    ContactUsItemComponent,
    GetKaitFormItemComponent,
    AnimationsComponent,
    RedirectButtonComponent,
    // USER-CODE-C
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(AppRoutes, { initialNavigation: false }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [AppService]
      }
    }),
    PipesModule,
    // USER-CODE-O
    // USER-CODE-C
  ],
  providers: [
    AppSettings,
    AppService,
    HttpService,
    TranslationService,
    // USER-CODE-O
    // USER-CODE-C
  ],
  entryComponents: [
    // USER-CODE-O
    // USER-CODE-C
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
    if (localStorage.getItem('frontAuthToken') === null) {
      const settings = new AppSettings();

      localStorage.setItem('frontAuthToken', settings.authGuestToken);
    }
  }
}

export class TranslationLoader implements TranslateLoader {

  constructor(private db: AppService) {}

  public getTranslation(lang): any {
    return this.db.getTranslations().pipe(map(res => res[lang])) as Observable<any>;
  }
}

export function HttpLoaderFactory(db: AppService) {
  return new TranslationLoader(db);
}
