import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'preview-component',
  template: '',
})
export class PreviewComponent {

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    activatedRoute.queryParams.subscribe(params => {
      if (params['previewUserToken']) {
        sessionStorage.setItem('previewMode', JSON.stringify(true));
        sessionStorage.setItem('previewAuthToken', params['previewUserToken']);
      }

      this.router.navigateByUrl('/');
    });
  }
}
