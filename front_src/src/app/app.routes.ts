import { PreviewComponent } from './preview.component';

import { TermsofserviceComponent } from './containers/termsofservice/termsofservice.component';
import { PrivacypolicyComponent } from './containers/privacypolicy/privacypolicy.component';
import { GetkaitComponent } from './containers/getkait/getkait.component';
import { LoginComponent } from './containers/login/login.component';
import { ContactComponent } from './containers/contact/contact.component';
import { HomeComponent } from './containers/home/home.component';

// USER-CODE-O
// USER-CODE-C

export const AppRoutes = [
  // USER-CODE-O
  // USER-CODE-C
  {
    path: 'enable-preview',
    component: PreviewComponent,
  },
  {
    path: 'terms-of-service',
    component: TermsofserviceComponent,
  },
  {
    path: 'privacy-policy',
    component: PrivacypolicyComponent,
  },
  {
    path: 'get-kait',
    component: GetkaitComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'contact',
    component: ContactComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },

  // USER-CODE-O
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
  // USER-CODE-C
];
