import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './../http.service';
import { QueryParams } from './../classes/QueryParams';
import { switchMap } from 'rxjs/operators';
import {
  ReachClientsIcons,
  ServicePackagesList,
  BenefitsList,
  FooterMenuList,
  Copyright,
  TopMenuLogo,
  TopMenuList,
  ContactFormReplyTo,
  ContactFormSettings,
  GetKaitSection,
  GetKaitReplyTo
} from './assemble.interface';
import { TranslateService } from '@ngx-translate/core';
import { TranslationService } from './../translation.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AssembleService {

  constructor(
    private httpService: HttpService,
    private translateService: TranslateService,
    private translationService: TranslationService,
    private router: Router
  ) { }

  /**
   * Pobranie assemble z ikonami dla service package
   *
   * @param  {string} lang
   * @retruns Observable<ServicePackagesList[]>
   */
  public getServicePackageList(lang: string): Observable<ServicePackagesList[]> {
    const params = new QueryParams();
    params.where('servicePackagesContainerListAssembleId', 1);

    const queryOptions = {
      params: params ? params.getParams() : {}
    };
    return this.httpService.get(lang + '/ServicePackagesList', queryOptions);
  }

  /**
   * Pobranie assemble z ikonami dla reach clients
   *
   * @param  {string} lang
   * @returns Observable<ReachClientsIcons[]>
   */
  public getReachClientsIconsList(lang: string): Observable<ReachClientsIcons[]> {
    const params = new QueryParams();
    params.where('reachClientsContainerIconsId', 1);

    const queryOptions = {
      params: params ? params.getParams() : {}
    };
    return this.httpService.get(lang + '/ReachClientsIcons', queryOptions);
  }

  /**
   * Pobranie assemble z ikonami dla service package
   *
   * @param  {string} lang
   * @returns Observable<BenefitsList[]>
   */
  public getBenefitsList(lang: string): Observable<BenefitsList[]> {
    const params = new QueryParams();
    params.where('benefitsContainerListAssembleId', 1);

    const queryOptions = {
      params: params ? params.getParams() : {}
    };
    return this.httpService.get(lang + '/BenefitsLists', queryOptions);
  }

  /**
   * Pobranie listy z linkami dla footer menu
   *
   * @param  {string} lang
   * @returns Observable<FooterMenuList[]>
   */
  public getFooterMenuList(lang: string): Observable<FooterMenuList[]> {
    return this.httpService.get(lang + '/FooterMenu');
  }

  /**
   * Pobranie copyright
   *
   * @param  {string} lang
   * @return Observable<Copyright[]>
   */
  public getCopyrightContent(lang: string): Observable<Copyright[]> {
    return this.httpService.get(lang + '/Copyright');
  }

  /**
   * Pobranie loga
   *
   * @param  {string} lang
   * @return Observable<TopMenuLogo[]>
   */
  public getTopMenuLogo(lang: string): Observable<TopMenuLogo> {
    return this.httpService.get(lang + '/TopMenuLogo/1');
  }

  /**
   * Pobranie listy elementów górnego menu
   *
   * @param  {string} lang
   * @return Observable<TopMenuList[]>
   */
  public getTopMenuList(lang: string): Observable<TopMenuList[]> {
    return this.httpService.get(lang + '/TopMenuList');
  }

  /**
   * Pobranie assemble z listą odbiorców maili dla formularza kontaktowego
   *
   * @param  {string} lang
   * @returns Observable<ContactFormReplyTo[]>
   */
  public getContactFormSettingsReplyTo(lang: string): Observable<ContactFormReplyTo[]> {
    const params = new QueryParams();
    params.where('contactFormSettingsReplyToListId', 1);

    const queryOptions = {
      params: params ? params.getParams() : {}
    };
    return this.httpService.get(lang + '/ContactFormReplyTo', queryOptions);
  }

  /**
   * Pobranie ustawien dla formularza kontaktowego
   *
   * @param  {string} lang
   * @returns Observable<ContactFormSettings[]>
   */
  public getContactFormSettings(lang: string): Observable<ContactFormSettings> {
    return this.httpService.get(lang + '/ContactFormSettings/1');
  }

  /**
   * Pobranie pól dla zakładki GetKait
   *
   * @param  {string} lang
   * @returns Observable<GetKaitSection>
   */
  public getKaitFields(lang: string): Observable<GetKaitSection> {
    return this.httpService.get(lang + '/GetKaitSection/1');
  }

  /**
   * Pobranie assemble z listą odbiorców maili dla formularza kontaktowego w Get Kait
   *
   * @param  {string} lang
   * @returns Observable<GetKaitReplyTo[]>
   */
  public getKaitReplyTo(lang: string): Observable<GetKaitReplyTo[]> {
    const params = new QueryParams();
    params.where('getKaitSectionFormReplyToListId', 1);

    const queryOptions = {
      params: params ? params.getParams() : {}
    };
    return this.httpService.get(lang + '/GetKaitReplyTo', queryOptions);
  }
}
