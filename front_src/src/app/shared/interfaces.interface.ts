// Interfaces for Contact page

export class FormFields {
  contactFormName: string;
  contactFormEmail: string;
  contactFormMessage: string;

  constructor() {
   this.contactFormName = '';
   this.contactFormEmail = '';
   this.contactFormMessage = '';
  }
}

export interface FormSendData {
  recipient: string;
  subject: string;
  senderName: string;
  senderAddress: string;
}

// Interfaces for GetKait page

export class GetKaitFormFields {
  getKaitFormBrandName: string;
  getKaitFormEmail: string;
  getKaitFormMobileNumber: string;
  getKaitFormContactPerson: string;
  getKaitFormSocial: string;

  constructor() {
    this.getKaitFormBrandName = '';
    this.getKaitFormEmail = '';
    this.getKaitFormMobileNumber = '';
    this.getKaitFormContactPerson = '';
    this.getKaitFormSocial = '';
  }
}

export interface GetKaitFormSend {
  recipient: string;
  subject: string;
  senderName: string;
  senderAddress: string;
}

// Interfaces for Login page

export class LoginFormFields {
  login: string;
  password: string;

  constructor() {
    this.login = '';
    this.password = '';
  }
}

export interface LoginFormSend {
  login: string;
  password: string;
}
