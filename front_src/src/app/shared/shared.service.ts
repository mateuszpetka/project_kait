import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  // Status wysylki formularza kontaktowego na stronie kontakt
  private _contactPageFormState: BehaviorSubject<string> = new BehaviorSubject<string>('');
  // Status wysylki formularza kontaktowego na stronie GetKait
  private _getkaitPageFormState: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor() { }

  /**
   * Ustawia status formularza na stronie kontaktowej
   * @param {string} state
   */
  setContactPageFormState(state: string): void {
    this._contactPageFormState.next(state);
  }

  /**
   * Sprawdza status formularza na stronie kontaktowej
   * @return {Observable<string>}
   */
  getContactPageFormState(): BehaviorSubject<string> {
    return this._contactPageFormState;
  }

  /**
   * Ustawia status formularza na stronie getkait
   * @param {string} state
   */
  setGetKaitPageFormState(state: string): void {
    this._getkaitPageFormState.next(state);
  }

  /**
   * Sprawdza status formularza na stronie kontaktowej
   * @return {Observable<string>}
   */
  getGetKaitPageFormState(): BehaviorSubject<string> {
    return this._getkaitPageFormState;
  }
}
