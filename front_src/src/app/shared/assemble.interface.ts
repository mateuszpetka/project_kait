export interface ReachClientsIcons {
  id?: number;
  status?: number;
  reachClientsContainerIconsId?: number;
  reachClientsIconsItemIcon?: string;
  reachClientsIconsItemPercent?: string;
}

export interface ServicePackagesList {
  id?: number;
  status?: number;
  servicePackagesListItemIcon?: string;
  servicePackagesListItemTitle?: string;
  servicePackagesListItemList?: Array<string>;
  servicePackagesListItemDescription?: string;
}

export interface BenefitsList {
  id?: number;
  status?: number;
  benefitItemImage?: string;
  benefitItemTitle?: string;
  benefitItemDescription?: string;
}

export interface FooterMenuList {
  id?: number;
  status?: number;
  footerMenuContainerUrl?: string;
  footerMenuContainerName?: string;
  footerMenuContainerTarget?: string;
}

export interface Copyright {
  id?: number;
  copyrightContainerTitle?: string;
  copyrightContainerDesc?: string;
}

export interface TopMenuList {
  id?: number;
  status?: number;
  topMenuListContainerName?: string;
  topMenuListContainerUrl?: string;
  topMenuListContainerTarget?: number;
  topMenuListContainerBorder?: number;
}

export interface TopMenuLogo {
  id?: number;
  topMenuLogoContainerImg?: string;
}

export interface ContactFormReplyTo {
  id?: number;
  status?: number;
  contactFormReplyToListId?: number;
  contactFormReplyToListEmail?: string;
  contactFormReplyToListName?: string;
}

export interface ContactFormSettings {
  id?: number;
  contactFormDescriptionHeader?: string;
  contactFormDescriptionDesc?: string;
  contactFormDescriptionEmail?: string;
  contactFormDescriptionSecondDesc?: string;
  contactFormSettingsRecipient?: string;
  contactFormSettingsSubject?: string;
  contactFormSettingsSenderAddress?: string;
  contactFormSettingsSenderName?: string;
  contactFormSettingsDescription?: string;
}

export interface GetKaitSection {
  id?: number;
  status?: number;
  getKaitSectionLeftBg: string;
  getKaitSectionLeftCenterImg: string;
  getKaitSectionFormTitle: string;
  getKaitSectionFormDescription: string;
  getKaitSectionFormRecipient: string;
  getKaitSectionFormSubject: string;
  getKaitSectionFormSenderAddress: string;
  getKaitSectionFormSenderName: string;
  getKaitSectionFormBottomBtnName: string;
  getKaitSectionFormBottomBtnUrl: string;
  getKaitSectionFormBottomDesc: string;
}

export interface GetKaitReplyTo {
  id?: number;
  status?: number;
  getKaitSectionFormReplyToListId?: number;
  getKaitReplyToName?: string;
  getKaitReplyToEmail?: string;
}
