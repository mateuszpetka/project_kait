import { AfterViewInit, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { filter } from 'rxjs/operators';

import { AppSettings } from './app.settings';
import { HttpService } from './http.service';
import { TranslationService } from './translation.service';

// USER-CODE-O
import AOS from 'aos'
import { AnimationsService } from "./components/custom/animations/animations.service";
// USER-CODE-C

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // USER-CODE-O
  // USER-CODE-C
})
export class AppComponent implements AfterViewInit {
  // USER-CODE-O
  public isInitialAnimation: boolean = true;
  // USER-CODE-C

  public previewMode = false;

  constructor(
    private router: Router,
    private http: HttpClient,
    private httpService: HttpService,
    private appSettings: AppSettings,
    private _translationService: TranslationService,
    private titleService: Title,
    // USER-CODE-O
    private animationsService: AnimationsService,
    // USER-CODE-C
  ) {
    if (this.httpService.isPreviewMode()) {
      this.previewMode = true;
    }

    if (localStorage.getItem('frontAuthToken') === null) {
      this.http.get(this.appSettings.apiUrl + '/Auth/getGuestToken').subscribe((res: any) => {
        const response = res;

        if (response.status === true) {
          localStorage.setItem('frontAuthToken', response.data.token);
        }
      });
    }

    this._translationService.ready.subscribe(ready => {
      if (ready) {
        this.router.initialNavigation();
      }
    })

    this.router.events.pipe(
      filter(event => event instanceof ActivationEnd)
    ).subscribe((event: ActivationEnd) => this.titleService.setTitle(event.snapshot.data.title));

    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  ngAfterViewInit(): void {
    this.animationsService.checkInitState().subscribe(res => {
      this.isInitialAnimation = res;
    });

    this.animationsService.checkAOSstate()
      .subscribe((state: boolean) => {
        if (state) {
          AOS.init();
        }
      });
  }
  // USER-CODE-C
}
