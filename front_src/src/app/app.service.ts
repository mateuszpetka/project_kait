import { AppSettings } from './app.settings';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { HttpService } from './http.service';

// USER-CODE-O
import { Title, Meta } from '@angular/platform-browser';
import { FilePathPipe } from './tools/pipes/file-path.pipe';
// USER-CODE-C

@Injectable()
export class AppService {
  // USER-CODE-O
  // USER-CODE-C
  constructor(
    private appSettings: AppSettings,
    private httpService: HttpService,
    // USER-CODE-O
    private titleService: Title,
    private metaService: Meta,
    private filePathPipe: FilePathPipe
    // USER-CODE-C
  ) {
    // USER-CODE-O
    // USER-CODE-C
  }

  public getTranslations() {
    const url = 'BlingTranslation';
    const queryOptions = {
      params: {
        'withJoin': 1
      }
    };

    return this.httpService.get(url, queryOptions).pipe(map(translations => {
      const parsedTranslations = [];
      
      translations.map(translation => {
        const keyValue = {[translation.translationKey]: translation.translation};
        const languageCode = this.appSettings['mapResponseJoins'] ? translation.language.code : translation.__language_code;

        if (parsedTranslations.hasOwnProperty(languageCode)) {
          parsedTranslations[languageCode] = {...parsedTranslations[languageCode], ...keyValue}
        } else {
          parsedTranslations[languageCode] = keyValue;
        }
      });

      return parsedTranslations;
    }));
  }

  // USER-CODE-O

  /**
   * Updates SEO information
   * @param  {string} title
   * @param  {string} description
   * @param  {string} image
   * @returns void
   */
  updateSEO(title: string, description: string, image: string): void {
    if (image) {
      const transformedImage = this.filePathPipe.transform(image);

      this.metaService.updateTag(
        { property: 'og:image', content: transformedImage },
        'property="og:image"'
      );

      this.metaService.updateTag(
        { itemprop: 'image', content: image },
        'itemprop="image"'
      );
    }

    this.titleService.setTitle(title);

    this.metaService.updateTag(
      { name: 'description', content: description },
      'name="description"'
    );

    this.metaService.updateTag(
      { property: 'og:description', content: description },
      'property="og:description"'
    );

    this.metaService.updateTag(
      { property: 'og:title', content: title },
      'property="og:title"'
    );

    this.metaService.updateTag(
      { itemprop: 'name', content: title },
      'itemprop="name"'
    );

    this.metaService.updateTag(
      { itemprop: 'description', content: description },
      'itemprop="description"'
    );
  }

  // USER-CODE-C
}
