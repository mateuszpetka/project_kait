export const BlingRouteStructure = Object.freeze({
  id: 'BlingRoute.`id`',
  name: 'BlingRoute.`name`',
  path: 'BlingRoute.`path`',
  title: 'BlingRoute.`title`',
  hasLanguagePrefix: 'BlingRoute.`hasLanguagePrefix`',
  params: 'BlingRoute.`params`',

});
