
export interface BlingRoute {
  id?: number;
  appId?: number;
  app?: any;
  name?: string;
  path?: string;
  title?: string;
  hasLanguagePrefix?: number;
  params?: object;

}
