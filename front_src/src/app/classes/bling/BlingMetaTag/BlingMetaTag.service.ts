import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { BlingMetaTag } from './BlingMetaTag';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BlingMetaTagService<T> extends Service<BlingMetaTag> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('BlingMetaTag', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
