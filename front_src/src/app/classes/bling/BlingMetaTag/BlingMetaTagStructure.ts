export const BlingMetaTagStructure = Object.freeze({
  id: 'BlingMetaTag.`id`',
  name: 'BlingMetaTag.`name`',
  value: 'BlingMetaTag.`value`',
  location: 'BlingMetaTag.`location`',

});
