
export interface BlingMetaTag {
  id?: number;
  appId?: number;
  app?: any;
  name?: string;
  value?: string;
  location?: string;

}
