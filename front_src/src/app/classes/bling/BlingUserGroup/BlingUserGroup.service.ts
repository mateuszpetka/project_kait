import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { BlingUserGroup } from './BlingUserGroup';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BlingUserGroupService<T> extends Service<BlingUserGroup> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('BlingUserGroup', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
