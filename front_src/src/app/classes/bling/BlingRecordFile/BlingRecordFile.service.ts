import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { BlingRecordFile } from './BlingRecordFile';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BlingRecordFileService<T> extends Service<BlingRecordFile> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('BlingRecordFile', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
