
export interface BlingRecordFile {
  id?: number;
  recordId?: number;
  fileId?: number;
  file?: any;
  entityFieldId?: number;
  entityField?: any;
  entity?: string;
  position?: number;

}
