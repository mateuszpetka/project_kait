import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { BlingRecordTag } from './BlingRecordTag';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BlingRecordTagService<T> extends Service<BlingRecordTag> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('BlingRecordTag', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
