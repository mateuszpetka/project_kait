
export interface BlingRecordTag {
  id?: number;
  recordId?: number;
  tagId?: number;
  tag?: any;
  entityFieldId?: number;
  entityField?: any;
  entity?: string;
  position?: number;

}
