export const BlingTranslationStructure = Object.freeze({
  id: 'BlingTranslation.`id`',
  languageAppId: 'BlingLanguage.`appId`',
  languageName: 'BlingLanguage.`name`',
  languageCode: 'BlingLanguage.`code`',
  languageActive: 'BlingLanguage.`active`',
  languageVisible: 'BlingLanguage.`visible`',
  languageIsDefault: 'BlingLanguage.`isDefault`',
  languagePosition: 'BlingLanguage.`position`',
  translationKey: 'BlingTranslation.`translationKey`',
  translation: 'BlingTranslation.`translation`',
  description: 'BlingTranslation.`description`',

});
