export const BlingMetaSnippetStructure = Object.freeze({
  id: 'BlingMetaSnippet.`id`',
  name: 'BlingMetaSnippet.`name`',
  value: 'BlingMetaSnippet.`value`',
  location: 'BlingMetaSnippet.`location`',

});
