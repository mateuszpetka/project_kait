import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { BlingMetaSnippet } from './BlingMetaSnippet';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BlingMetaSnippetService<T> extends Service<BlingMetaSnippet> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('BlingMetaSnippet', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
