
export interface BlingMetaSnippet {
  id?: number;
  name?: string;
  value?: string;
  location?: string;

}
