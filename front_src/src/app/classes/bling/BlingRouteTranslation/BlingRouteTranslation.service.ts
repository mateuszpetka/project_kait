import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { BlingRouteTranslation } from './BlingRouteTranslation';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BlingRouteTranslationService<T> extends Service<BlingRouteTranslation> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('BlingRouteTranslation', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
