
export interface BlingRouteTranslation {
  id?: number;
  routeId?: number;
  route?: any;
  languageId?: number;
  language?: any;
  translationKey?: string;
  translation?: string;
  title?: string;
  params?: object;

}
