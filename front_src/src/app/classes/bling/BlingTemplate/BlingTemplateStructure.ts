export const BlingTemplateStructure = Object.freeze({
  id: 'BlingTemplate.`id`',
  name: 'BlingTemplate.`name`',
  displayName: 'BlingTemplate.`displayName`',
  description: 'BlingTemplate.`description`',
  tags: 'BlingTemplate.`tags`',
  subject: 'BlingTemplate.`subject`',
  htmlContent: 'BlingTemplate.`htmlContent`',
  parentName: 'BlingTemplate.`name`',
  parentDisplayName: 'BlingTemplate.`displayName`',
  parentDescription: 'BlingTemplate.`description`',
  parentTags: 'BlingTemplate.`tags`',
  parentSubject: 'BlingTemplate.`subject`',
  parentHtmlContent: 'BlingTemplate.`htmlContent`',
  parentParentId: 'BlingTemplate.`parentId`',
  parentKind: 'BlingTemplate.`kind`',
  kind: 'BlingTemplate.`kind`',

});
