
export interface BlingTemplate {
  id?: number;
  name?: string;
  displayName?: string;
  description?: string;
  tags?: object;
  subject?: string;
  htmlContent?: string;
  parentId?: number;
  parent?: any;
  kind?: string;

}
