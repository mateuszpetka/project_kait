import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { BlingTemplate } from './BlingTemplate';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BlingTemplateService<T> extends Service<BlingTemplate> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('BlingTemplate', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
