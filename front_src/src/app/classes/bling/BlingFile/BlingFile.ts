
export interface BlingFile {
  id?: number;
  name?: string;
  path?: string;
  title?: string;
  alt?: string;
  size?: number;
  mime?: string;
  width?: number;
  height?: number;
  duration?: number;
  status?: number;
  isForeign?: number;
  params?: object;

}
