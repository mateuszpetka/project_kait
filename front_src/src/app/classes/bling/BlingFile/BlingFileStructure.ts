export const BlingFileStructure = Object.freeze({
  id: 'BlingFile.`id`',
  name: 'BlingFile.`name`',
  path: 'BlingFile.`path`',
  title: 'BlingFile.`title`',
  alt: 'BlingFile.`alt`',
  size: 'BlingFile.`size`',
  mime: 'BlingFile.`mime`',
  width: 'BlingFile.`width`',
  height: 'BlingFile.`height`',
  duration: 'BlingFile.`duration`',
  status: 'BlingFile.`status`',
  isForeign: 'BlingFile.`isForeign`',
  params: 'BlingFile.`params`',

});
