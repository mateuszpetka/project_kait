
export interface BlingLanguage {
  id?: number;
  appId?: number;
  app?: any;
  name?: string;
  code?: string;
  active?: number;
  visible?: number;
  isDefault?: number;
  position?: number;

}
