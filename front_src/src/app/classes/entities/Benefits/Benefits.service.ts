import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { Benefits } from './Benefits';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BenefitsService<T> extends Service<Benefits> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('Benefits', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
