
export interface Benefits {
  id?: number;
  benefitsContainerListAssemble?: number;
  benefitsContainerTitle?: string;
  benefitsContainerBtnName?: string;
  benefitsContainerBtnUrl?: string;

}
