export const BenefitsStructure = Object.freeze({
  id: 'Benefits.`id`',
  benefitsContainerListAssemble: 'Benefits.`benefitsContainerListAssemble`',
  benefitsContainerTitle: 'Benefits.`benefitsContainerTitle`',
  benefitsContainerBtnName: 'Benefits.`benefitsContainerBtnName`',
  benefitsContainerBtnUrl: 'Benefits.`benefitsContainerBtnUrl`',

});
