
export interface Copyright {
  id?: number;
  copyrightContainerTitle?: string;
  copyrightContainerDesc?: string;
  seoTitle?: string;
  seoDescription?: string;
  seoImage?: string;

}
