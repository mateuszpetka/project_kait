export const CopyrightStructure = Object.freeze({
  id: 'Copyright.`id`',
  copyrightContainerTitle: 'Copyright.`copyrightContainerTitle`',
  copyrightContainerDesc: 'Copyright.`copyrightContainerDesc`',
  seoTitle: 'Copyright.`seoTitle`',
  seoDescription: 'Copyright.`seoDescription`',
  seoImage: 'Copyright.`seoImage`',

});
