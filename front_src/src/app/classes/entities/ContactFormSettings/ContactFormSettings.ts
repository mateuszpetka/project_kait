
export interface ContactFormSettings {
  id?: number;
  contactFormSettingsRecipient?: string;
  contactFormSettingsSubject?: string;
  contactFormSettingsSenderName?: string;
  contactFormSettingsSenderAddress?: string;
  contactFormDescriptionHeader?: string;
  contactFormDescriptionDesc?: string;
  contactFormDescriptionEmail?: string;
  contactFormDescriptionSecondDesc?: string;
  contactFormSettingsReplyToList?: number;
  contactFormSettingsDescription?: string;

}
