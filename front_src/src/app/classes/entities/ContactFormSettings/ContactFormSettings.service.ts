import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { ContactFormSettings } from './ContactFormSettings';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class ContactFormSettingsService<T> extends Service<ContactFormSettings> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('ContactFormSettings', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
