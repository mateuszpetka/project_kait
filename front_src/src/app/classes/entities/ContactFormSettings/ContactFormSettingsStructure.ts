export const ContactFormSettingsStructure = Object.freeze({
  id: 'ContactFormSettings.`id`',
  contactFormSettingsRecipient: 'ContactFormSettings.`contactFormSettingsRecipient`',
  contactFormSettingsSubject: 'ContactFormSettings.`contactFormSettingsSubject`',
  contactFormSettingsSenderName: 'ContactFormSettings.`contactFormSettingsSenderName`',
  contactFormSettingsSenderAddress: 'ContactFormSettings.`contactFormSettingsSenderAddress`',
  contactFormDescriptionHeader: 'ContactFormSettings.`contactFormDescriptionHeader`',
  contactFormDescriptionDesc: 'ContactFormSettings.`contactFormDescriptionDesc`',
  contactFormDescriptionEmail: 'ContactFormSettings.`contactFormDescriptionEmail`',
  contactFormDescriptionSecondDesc: 'ContactFormSettings.`contactFormDescriptionSecondDesc`',
  contactFormSettingsReplyToList: 'ContactFormSettings.`contactFormSettingsReplyToList`',
  contactFormSettingsDescription: 'ContactFormSettings.`contactFormSettingsDescription`',

});
