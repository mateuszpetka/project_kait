export const GetKaitSectionStructure = Object.freeze({
  id: 'GetKaitSection.`id`',
  getKaitSectionLeftBg: 'GetKaitSection.`getKaitSectionLeftBg`',
  getKaitSectionLeftCenterImg: 'GetKaitSection.`getKaitSectionLeftCenterImg`',
  getKaitSectionFormTitle: 'GetKaitSection.`getKaitSectionFormTitle`',
  getKaitSectionFormDescription: 'GetKaitSection.`getKaitSectionFormDescription`',

});
