import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { GetKaitSection } from './GetKaitSection';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class GetKaitSectionService<T> extends Service<GetKaitSection> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('GetKaitSection', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
