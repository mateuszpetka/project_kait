
export interface GetKaitSection {
  id?: number;
  getKaitSectionLeftBg?: string;
  getKaitSectionLeftCenterImg?: string;
  getKaitSectionFormTitle?: string;
  getKaitSectionFormDescription?: string;

}
