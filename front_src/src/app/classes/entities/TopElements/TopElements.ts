
export interface TopElements {
  id?: number;
  topSectionDescription?: string;
  topSectionImage?: string;
  topSectionButtonText?: string;
  topSectionButtonLink?: string;
  seoTitle?: string;
  seoDescription?: string;
  seoImage?: string;
  topSectionHeader?: string;

}
