import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { TopElements } from './TopElements';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class TopElementsService<T> extends Service<TopElements> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('TopElements', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
