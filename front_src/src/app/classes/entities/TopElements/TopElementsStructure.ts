export const TopElementsStructure = Object.freeze({
  id: 'TopElements.`id`',
  topSectionDescription: 'TopElements.`topSectionDescription`',
  topSectionImage: 'TopElements.`topSectionImage`',
  topSectionButtonText: 'TopElements.`topSectionButtonText`',
  topSectionButtonLink: 'TopElements.`topSectionButtonLink`',
  seoTitle: 'TopElements.`seoTitle`',
  seoDescription: 'TopElements.`seoDescription`',
  seoImage: 'TopElements.`seoImage`',
  topSectionHeader: 'TopElements.`topSectionHeader`',

});
