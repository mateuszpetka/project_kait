
export interface BottomDescription {
  id?: number;
  bottomDescriptionContainerImage?: string;
  bottomDescriptionContainerChat?: Array<string>;
  bottomDescriptionContainerBtnName?: string;
  bottomDescriptionContainerBtnUrl?: string;

}
