export const BottomDescriptionStructure = Object.freeze({
  id: 'BottomDescription.`id`',
  bottomDescriptionContainerImage: 'BottomDescription.`bottomDescriptionContainerImage`',
  bottomDescriptionContainerChat: 'BottomDescription.`bottomDescriptionContainerChat`',
  bottomDescriptionContainerBtnName: 'BottomDescription.`bottomDescriptionContainerBtnName`',
  bottomDescriptionContainerBtnUrl: 'BottomDescription.`bottomDescriptionContainerBtnUrl`',

});
