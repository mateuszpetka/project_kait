import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { BottomDescription } from './BottomDescription';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class BottomDescriptionService<T> extends Service<BottomDescription> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('BottomDescription', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
