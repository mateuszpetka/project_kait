export const SocialMediaStructure = Object.freeze({
  id: 'SocialMedia.`id`',
  status: 'SocialMedia.`status`',
  socialMediaIcon: 'SocialMedia.`socialMediaIcon`',
  socialMediaUrl: 'SocialMedia.`socialMediaUrl`',
  socialMediaShowOnFooter: 'SocialMedia.`socialMediaShowOnFooter`',

});
