
export interface SocialMedia {
  id?: number;
  status?: number;
  socialMediaIcon?: string;
  socialMediaUrl?: string;
  socialMediaShowOnFooter?: number;

}
