import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { SocialMedia } from './SocialMedia';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class SocialMediaService<T> extends Service<SocialMedia> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('SocialMedia', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
