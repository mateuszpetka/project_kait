import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { PrivacyPolicy } from './PrivacyPolicy';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class PrivacyPolicyService<T> extends Service<PrivacyPolicy> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('PrivacyPolicy', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
