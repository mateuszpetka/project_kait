export const PrivacyPolicyStructure = Object.freeze({
  id: 'PrivacyPolicy.`id`',
  privacyPolicyDescription: 'PrivacyPolicy.`privacyPolicyDescription`',
  seoTitle: 'PrivacyPolicy.`seoTitle`',
  seoDescription: 'PrivacyPolicy.`seoDescription`',
  seoImage: 'PrivacyPolicy.`seoImage`',

});
