
export interface PrivacyPolicy {
  id?: number;
  privacyPolicyDescription?: string;
  seoTitle?: string;
  seoDescription?: string;
  seoImage?: string;

}
