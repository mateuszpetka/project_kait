export const CustomerServiceStructure = Object.freeze({
  id: 'CustomerService.`id`',
  customerServiceFieldsTitle: 'CustomerService.`customerServiceFieldsTitle`',
  customerServiceFieldsDescription: 'CustomerService.`customerServiceFieldsDescription`',
  customerServiceFieldsBtnName: 'CustomerService.`customerServiceFieldsBtnName`',
  customerServiceFieldsBtnUrl: 'CustomerService.`customerServiceFieldsBtnUrl`',
  customerServiceChatBg: 'CustomerService.`customerServiceChatBg`',

});
