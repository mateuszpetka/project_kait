
export interface CustomerService {
  id?: number;
  customerServiceFieldsTitle?: string;
  customerServiceFieldsDescription?: string;
  customerServiceFieldsBtnName?: string;
  customerServiceFieldsBtnUrl?: string;
  customerServiceChatBg?: string;

}
