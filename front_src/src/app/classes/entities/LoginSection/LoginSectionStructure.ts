export const LoginSectionStructure = Object.freeze({
  id: 'LoginSection.`id`',
  loginSectionImg: 'LoginSection.`loginSectionImg`',
  loginSectionFormTitle: 'LoginSection.`loginSectionFormTitle`',
  loginSectionBtnName: 'LoginSection.`loginSectionBtnName`',
  loginSectionBtnDesc: 'LoginSection.`loginSectionBtnDesc`',
  loginSectionBtnLink: 'LoginSection.`loginSectionBtnLink`',
  loginSectionFormForgotTitle: 'LoginSection.`loginSectionFormForgotTitle`',
  loginSectionForgotImg: 'LoginSection.`loginSectionForgotImg`',
  seoTitle: 'LoginSection.`seoTitle`',
  seoDescription: 'LoginSection.`seoDescription`',
  seoImage: 'LoginSection.`seoImage`',

});
