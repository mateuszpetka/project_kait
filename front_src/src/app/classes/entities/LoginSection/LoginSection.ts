
export interface LoginSection {
  id?: number;
  loginSectionImg?: string;
  loginSectionFormTitle?: string;
  loginSectionBtnName?: string;
  loginSectionBtnDesc?: string;
  loginSectionBtnLink?: string;
  loginSectionFormForgotTitle?: string;
  loginSectionForgotImg?: string;
  seoTitle?: string;
  seoDescription?: string;
  seoImage?: string;

}
