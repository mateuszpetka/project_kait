
export interface ReachClients {
  id?: number;
  reachClientsContainerIconsDesc?: string;
  reachClientsContainerHeader?: string;
  reachClientsContainerDescription?: string;
  reachClientsContainerIcons?: number;
  reachClientsContainerBtnName?: string;
  reachClientsContainerBtnUrl?: string;

}
