export const ReachClientsStructure = Object.freeze({
  id: 'ReachClients.`id`',
  reachClientsContainerIconsDesc: 'ReachClients.`reachClientsContainerIconsDesc`',
  reachClientsContainerHeader: 'ReachClients.`reachClientsContainerHeader`',
  reachClientsContainerDescription: 'ReachClients.`reachClientsContainerDescription`',
  reachClientsContainerIcons: 'ReachClients.`reachClientsContainerIcons`',
  reachClientsContainerBtnName: 'ReachClients.`reachClientsContainerBtnName`',
  reachClientsContainerBtnUrl: 'ReachClients.`reachClientsContainerBtnUrl`',

});
