
export interface ServicePackages {
  id?: number;
  servicePackagesContainerBtnUrl?: string;
  servicePackagesContainerButtonName?: string;
  servicePackagesContainerListAssemble?: number;
  servicePackagesContainerTitle?: string;

}
