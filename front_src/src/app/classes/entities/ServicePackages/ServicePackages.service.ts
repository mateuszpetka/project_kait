import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { ServicePackages } from './ServicePackages';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class ServicePackagesService<T> extends Service<ServicePackages> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('ServicePackages', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
