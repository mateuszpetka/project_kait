export const ServicePackagesStructure = Object.freeze({
  id: 'ServicePackages.`id`',
  servicePackagesContainerBtnUrl: 'ServicePackages.`servicePackagesContainerBtnUrl`',
  servicePackagesContainerButtonName: 'ServicePackages.`servicePackagesContainerButtonName`',
  servicePackagesContainerListAssemble: 'ServicePackages.`servicePackagesContainerListAssemble`',
  servicePackagesContainerTitle: 'ServicePackages.`servicePackagesContainerTitle`',

});
