import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { GetKaitImages } from './GetKaitImages';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class GetKaitImagesService<T> extends Service<GetKaitImages> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('GetKaitImages', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
