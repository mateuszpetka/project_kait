import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { GetKaitForm } from './GetKaitForm';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class GetKaitFormService<T> extends Service<GetKaitForm> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('GetKaitForm', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
