export const TermsOfServiceStructure = Object.freeze({
  id: 'TermsOfService.`id`',
  termsOfServiceDescription: 'TermsOfService.`termsOfServiceDescription`',
  seoTitle: 'TermsOfService.`seoTitle`',
  seoDescription: 'TermsOfService.`seoDescription`',
  seoImage: 'TermsOfService.`seoImage`',

});
