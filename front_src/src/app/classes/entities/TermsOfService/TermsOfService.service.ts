import { Injectable } from '@angular/core';

import { HttpService } from '../../../http.service';

import { TermsOfService } from './TermsOfService';
import { Service } from '../../../tools/models/service.model';

// USER-CODE-O
// USER-CODE-C

@Injectable()
export class TermsOfServiceService<T> extends Service<TermsOfService> {

  // USER-CODE-O
  // USER-CODE-C

  constructor(
    public httpService: HttpService,
    // USER-CODE-O
    // USER-CODE-C
  ) {
    super('TermsOfService', httpService);
    // USER-CODE-O
    // USER-CODE-C
  }

  // USER-CODE-O
  // USER-CODE-C
}
