
export interface TermsOfService {
  id?: number;
  termsOfServiceDescription?: string;
  seoTitle?: string;
  seoDescription?: string;
  seoImage?: string;

}
