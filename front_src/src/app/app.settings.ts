import { environment } from '../environments/environment';

export class AppSettings {
  public apiIp = location.protocol + '//' + environment.location;
  public apiUrl = this.apiIp + '/api';
  public authGuestToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJBUEkgdG9rZW4gcHJvdmlkZXIiLCJpYXQiOjE1NTYyNzM5ODYsImV4cCI6MzEzMzA3Mzk4NiwiaWQiOjAsImxvZ2luIjoiZ3Vlc3QiLCJncm91cElkIjoxMH0.a3RiksMEZN-FfZ2Ruc_1vPK7RwoWFUDCEjHBWRnxVSs';
  public mapResponseJoins = true;
}
