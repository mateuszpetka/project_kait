import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { Copyright } from '../../../classes/entities/Copyright/Copyright';
import { CopyrightService } from '../../../classes/entities/Copyright/Copyright.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'copyright-item',
  templateUrl: './copyright-item.component.html',
  styleUrls: ['./copyright-item.component.scss'],
  providers: [CopyrightService]
})
export class CopyrightItemComponent extends ItemComponentModel<Copyright> implements OnInit {
  public data$: Observable<Copyright>;

  constructor(
    private copyrightService: CopyrightService<Copyright>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(copyrightService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
