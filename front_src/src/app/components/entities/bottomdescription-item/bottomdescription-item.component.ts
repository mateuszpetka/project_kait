import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BottomDescription } from '../../../classes/entities/BottomDescription/BottomDescription';
import { BottomDescriptionService } from '../../../classes/entities/BottomDescription/BottomDescription.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'bottomdescription-item',
  templateUrl: './bottomdescription-item.component.html',
  styleUrls: ['./bottomdescription-item.component.scss'],
  providers: [BottomDescriptionService]
})
export class BottomDescriptionItemComponent extends ItemComponentModel<BottomDescription> implements OnInit {
  public data$: Observable<BottomDescription>;

  constructor(
    private bottomDescriptionService: BottomDescriptionService<BottomDescription>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(bottomDescriptionService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
