import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { GetKaitForm } from '../../../classes/entities/GetKaitForm/GetKaitForm';
import { GetKaitFormService } from '../../../classes/entities/GetKaitForm/GetKaitForm.service';

import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, takeUntil, tap } from "rxjs/operators";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { FormFields, GetKaitFormFields, GetKaitFormSend } from "../../../shared/interfaces.interface";
import { GetKaitReplyTo, GetKaitSection } from "../../../shared/assemble.interface";
import { AssembleService } from "../../../shared/assemble.service";
import { HttpService } from "../../../http.service";
import { SharedService } from "../../../shared/shared.service";

@Component({
  selector: 'getkaitform-item',
  templateUrl: './getkaitform-item.component.html',
  styleUrls: ['./getkaitform-item.component.scss'],
  providers: [GetKaitFormService]
})
export class GetKaitFormItemComponent extends ItemComponentModel<GetKaitForm> implements OnInit {
  public descriptionData$: Observable<GetKaitSection>;
  private getKaitReplyTo$: Observable<GetKaitReplyTo[]>;
  public lang: string;
  public contactForm: FormGroup;
  public alertType: string = '';
  public formErrors: GetKaitFormFields = new GetKaitFormFields();

  private contactFormSendData: GetKaitFormSend;
  private langSubscribe: Subscription;
  private validationMessages = {
    getKaitFormBrandName: {
      required: 'form.errors.brandname.required',
      minlength: 'form.errors.brandname.minlength'
    },
    getKaitFormEmail: {
      email: 'form.errors.email.validmail',
      required: 'form.errors.email.required'
    },
    getKaitFormMobileNumber: {
      required: 'form.errors.phone.required',
      pattern: 'form.errors.phone.pattern',
    },
    getKaitFormContactPerson: {
      required: 'form.errors.contactperson.required',
      minlength: 'form.errors.contactperson.minlength'
    },
    getKaitFormSocial: {
      required: 'form.errors.social.required',
      minlength: 'form.errors.social.minlength'
    }
  };
  private _destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private getKaitFormService: GetKaitFormService<GetKaitForm>,
    private assembleService: AssembleService,
    private formBuilder: FormBuilder,
    private http: HttpService,
    public translationService: TranslationService
  ) {
    super(getKaitFormService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();

    this.getAssemble();
    this.assignContactFormSettings();

    /**
     * Podstawowe validatory
     * https://angular.io/api/forms/Validators
     */
    this.contactForm = this.formBuilder.group({
      getKaitFormBrandName: ['', [
        Validators.required,
        Validators.minLength(3)
        ]
      ],
      getKaitFormEmail: ['', [
        Validators.email,
        Validators.required
        ]
      ],
      getKaitFormMobileNumber: ['', [
        Validators.pattern(new RegExp(/^[\\+]?[0-9]{8,}$/im)),
        Validators.required
        ]
      ],
      getKaitFormContactPerson: ['', [
        Validators.required,
        Validators.minLength(3)
        ]
      ],
      getKaitFormSocial: ['', [
        Validators.required,
        Validators.minLength(3)
        ]
      ]
    });

    /**
     * Obserwowanie czy wartość w kontrolkach się zmieniła i walidowanie
     */
    this.contactForm.valueChanges.pipe(
      takeUntil(this._destroy$),
      debounceTime(500)
    ).subscribe((value: FormFields) => {
      this.onControlValueChanged();
    });

    this.onControlValueChanged();
  }

  /**
   * Pobieranie i przypisywanie assembli w zależności od języka
   */
  getAssemble(): void {
    this.translationService.currentLangObservator.pipe(
      takeUntil(this._destroy$)
    ).subscribe((lang) => {
      this.lang = lang;
      this.descriptionData$ = this.assembleService.getKaitFields(lang);
      this.getKaitReplyTo$ = this.assembleService.getKaitReplyTo(lang);
    });
  }

  /**
   * Przypisywanie wymaganych pól do wysyłki formularza
   */
  assignContactFormSettings(): void {
    this.descriptionData$.pipe(
      takeUntil(this._destroy$)
    ).subscribe((res: GetKaitSection) => {
      this.contactFormSendData = {
        recipient: res.getKaitSectionFormRecipient,
        subject: res.getKaitSectionFormSubject,
        senderName: res.getKaitSectionFormSenderName,
        senderAddress: res.getKaitSectionFormSenderAddress
      }
    });
  }

  /**
   * Wysyłka formularza kontaktowego na maila
   * @param {FormGroup} form
   */
  sendContactFormToMail(form: FormGroup): void {
    const body = new FormData();

    body.append('recipientEmail', `${this.contactFormSendData.recipient}`);
    body.append('subject', `${this.contactFormSendData.subject}`);
    body.append('senderName', `${this.contactFormSendData.senderName}`);
    body.append('senderAddress', `${this.contactFormSendData.senderAddress}`);
    body.append('htmlContent', `
      <ul style="list-style-type: none; padding: 0">
        <li><strong>Brand Name:</strong> ${form.value.getKaitFormBrandName}</li>
        <li><strong>E-mail:</strong> ${form.value.getKaitFormEmail}</li>
        <li><strong>Phone number:</strong> ${form.value.getKaitFormMobileNumber}</li>
        <li><strong>Contact Person:</strong> ${form.value.getKaitFormContactPerson}</li>
        <li><strong>WWW / Social:</strong> ${form.value.getKaitFormSocial}</li>
      </ul>
    `);

    this.getKaitReplyTo$.subscribe(res => {
      for (let item of res) {
        const replyToMail: string = item.getKaitReplyToEmail;
        const replyToName: string = item.getKaitReplyToName;
        body.append(`replyTo[${replyToMail}]`, `${replyToName}`);
      }

      this.http.post('Contact/mailToRecipient', body).subscribe(
        res => {},
        error => {
          console.error('error', error.error.msg);
        }
      );
    })
  }

  /**
   * Akcja na wysyłkę formularza kontaktowego
   * @param {FormGroup} form
   */
  onSubmit(form: FormGroup): void {
    if (this.contactForm.valid) {
      this.sendContactFormToMail(form);
      this.http.post('GetKaitFormList', { ...this.contactForm.getRawValue(), status: 1 })
        .subscribe(res => {
          if (res.status) {
            this.contactForm.reset();
            this.generateAlert('success');
          } else {
            this.generateAlert('error');
          }
        });
    } else {
      this.formSubmitValidation();
    }
  };

  /**
   * Sprawdzanie kontrolek podczas wpisywania
   */
  onControlValueChanged(): void {
    const form = this.contactForm;

    for (let field in this.formErrors) {
      this.formErrors[field] = '';
      let control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const validationMessages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key];
        }
      }
    }
  }

  /**
   * Sprawdzanie kontrolek podczas wysyłki
   */
  formSubmitValidation(): void {
    const form = this.contactForm;

    for (let field in this.formErrors) {
      this.formErrors[field] = '';
      let control = form.get(field);

      if (control && !control.valid) {
        const validationMessages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key];
        }
      }
    }
  }

  /**
   * Generowanie alertu o prawidłowej wysyłce
   * @param {string} type
   */
  generateAlert(type: string): void {
    document.querySelector('.container-form').classList.add('send-state');
    this.alertType = type;
  }

  ngOnDestroy() {
    this._destroy$.next();
  }
}
