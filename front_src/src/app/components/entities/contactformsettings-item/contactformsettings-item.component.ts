import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { ContactFormSettings } from '../../../classes/entities/ContactFormSettings/ContactFormSettings';
import { ContactFormSettingsService } from '../../../classes/entities/ContactFormSettings/ContactFormSettings.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'contactformsettings-item',
  templateUrl: './contactformsettings-item.component.html',
  styleUrls: ['./contactformsettings-item.component.scss'],
  providers: [ContactFormSettingsService]
})
export class ContactFormSettingsItemComponent extends ItemComponentModel<ContactFormSettings> implements OnInit {
  public data$: Observable<ContactFormSettings>;

  constructor(
    private contactFormSettingsService: ContactFormSettingsService<ContactFormSettings>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(contactFormSettingsService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
