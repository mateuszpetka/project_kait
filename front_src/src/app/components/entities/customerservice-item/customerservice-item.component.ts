import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { CustomerService } from '../../../classes/entities/CustomerService/CustomerService';
import { CustomerServiceService } from '../../../classes/entities/CustomerService/CustomerService.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'customerservice-item',
  templateUrl: './customerservice-item.component.html',
  styleUrls: ['./customerservice-item.component.scss'],
  providers: [CustomerServiceService]
})
export class CustomerServiceItemComponent extends ItemComponentModel<CustomerService> implements OnInit {
  public data$: Observable<CustomerService>;

  constructor(
    private customerServiceService: CustomerServiceService<CustomerService>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(customerServiceService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
