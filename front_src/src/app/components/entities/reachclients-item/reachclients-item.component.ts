import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { ReachClients } from '../../../classes/entities/ReachClients/ReachClients';
import { ReachClientsService } from '../../../classes/entities/ReachClients/ReachClients.service';

import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ReachClientsIcons } from '../../../shared/assemble.interface';
import { AssembleService } from "../../../shared/assemble.service";

@Component({
  selector: 'reachclients-item',
  templateUrl: './reachclients-item.component.html',
  styleUrls: ['./reachclients-item.component.scss'],
  providers: [ReachClientsService]
})
export class ReachClientsItemComponent extends ItemComponentModel<ReachClients> implements OnInit {
  public data$: Observable<ReachClients>;
  public reachClientsIconsList$: Observable<ReachClientsIcons[]>;

  constructor(
    private reachClientsService: ReachClientsService<ReachClients>,
    private assembleService: AssembleService,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(reachClientsService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );

    this.reachClientsIconsList$ = this.translationService.currentLangObservator.pipe(
      switchMap((lang) => this.assembleService.getReachClientsIconsList(lang))
    );
  }
}
