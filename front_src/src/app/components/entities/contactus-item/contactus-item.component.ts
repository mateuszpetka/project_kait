import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { ContactUs } from '../../../classes/entities/ContactUs/ContactUs';
import { ContactUsService } from '../../../classes/entities/ContactUs/ContactUs.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'contactus-item',
  templateUrl: './contactus-item.component.html',
  styleUrls: ['./contactus-item.component.scss'],
  providers: [ContactUsService]
})
export class ContactUsItemComponent extends ItemComponentModel<ContactUs> implements OnInit {
  public data$: Observable<ContactUs>;

  constructor(
    private contactUsService: ContactUsService<ContactUs>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(contactUsService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
