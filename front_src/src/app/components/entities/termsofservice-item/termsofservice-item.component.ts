import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { TermsOfService } from '../../../classes/entities/TermsOfService/TermsOfService';
import { TermsOfServiceService } from '../../../classes/entities/TermsOfService/TermsOfService.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'termsofservice-item',
  templateUrl: './termsofservice-item.component.html',
  styleUrls: ['./termsofservice-item.component.scss'],
  providers: [TermsOfServiceService]
})
export class TermsOfServiceItemComponent extends ItemComponentModel<TermsOfService> implements OnInit {
  public data$: Observable<TermsOfService>;

  constructor(
    private termsOfServiceService: TermsOfServiceService<TermsOfService>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(termsOfServiceService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
