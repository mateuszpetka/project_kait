import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { SocialMedia } from '../../../classes/entities/SocialMedia/SocialMedia';
import { SocialMediaService } from '../../../classes/entities/SocialMedia/SocialMedia.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'socialmedia-item',
  templateUrl: './socialmedia-item.component.html',
  styleUrls: ['./socialmedia-item.component.scss'],
  providers: [SocialMediaService]
})
export class SocialMediaItemComponent extends ItemComponentModel<SocialMedia> implements OnInit {
  public data$: Observable<SocialMedia>;

  constructor(
    private socialMediaService: SocialMediaService<SocialMedia>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(socialMediaService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
