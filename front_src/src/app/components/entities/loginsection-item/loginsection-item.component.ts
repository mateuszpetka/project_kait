import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { LoginSection } from '../../../classes/entities/LoginSection/LoginSection';
import { LoginSectionService } from '../../../classes/entities/LoginSection/LoginSection.service';

import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime,takeUntil, tap } from "rxjs/operators";
import { LoginFormSend, LoginFormFields } from "../../../shared/interfaces.interface";
import { FormBuilder, FormGroup, Validators }  from "@angular/forms";
import { HttpService } from "../../../http.service";

@Component({
  selector: 'loginsection-item',
  templateUrl: './loginsection-item.component.html',
  styleUrls: ['./loginsection-item.component.scss'],
  providers: [LoginSectionService]
})
export class LoginSectionItemComponent extends ItemComponentModel<LoginSection> implements OnInit {
  public data$: Observable<LoginSection>;
  public formErrors: LoginFormFields  = new LoginFormFields();
  public contactForm: FormGroup;
  public recoveryForm: FormGroup;
  public alertType: string = '';
  public isRecoveryFormVisible = false;
  public isRecoveryFormSent = false;

  private contactFormSendData: LoginFormSend;
  private langSubscribe: Subscription;
  private validationMessages = {
    login: {
      required: 'form.errors.login.required',
      minlength: 'form.errors.login.minlength'
    },
    password: {
      required: 'form.errors.password.required',
      minlength: 'form.errors.password.minlength'
    }
  };
  private _destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private loginSectionService: LoginSectionService<LoginSection>,
    private formBuilder: FormBuilder,
    private http: HttpService,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(loginSectionService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );

    /**
     * Podstawowe validatory
     * https://angular.io/api/forms/Validators
     */
    this.contactForm = this.formBuilder.group({
      login: ['', [
        Validators.required,
        Validators.minLength(3)
        ]
      ],
      password: ['', [
        Validators.required,
        Validators.minLength(3)
        ]
      ],
    });

    this.recoveryForm = this.formBuilder.group({
      login: ['', [
        Validators.required,
        Validators.minLength(3)
        ]
      ],
    });

    /**
     * Obserwowanie czy wartość w kontrolkach się zmieniła i walidowanie
     */
    this.contactForm.valueChanges.pipe(
      takeUntil(this._destroy$),
      debounceTime(500)
    ).subscribe( (value: LoginFormFields) => {
      this.onControlValueChanged();
    });

    this.onControlValueChanged();
  }

  /**
   * Wysyłka formularza kontaktowego na maila
   * @param form
   */
  sendContactFormToMail(form: FormGroup): void {
    const body = new FormData();

    this.http.post('Contact/mailToRecipient', body).subscribe(
      res => {
      },
      (error) => {
        console.error('error', error.error.msg);
      },
      () => {
      }
    );
  }

  /**
   * Akcja na wysyłkę formularza kontaktowego
   * @param form
   */
  onSubmit(form: FormGroup): void {
    if (this.contactForm.valid) {
      //TODO do edycji czekamy na info od nich
      console.log(this.contactForm.getRawValue());
    }
  };

  /**
   * Akcja na wysylke formularza z odsyskaniem hasła
   * @param {FormGroup} form
   */
  onSubmitRecovery(form: FormGroup): void {
    if (this.recoveryForm.valid) {
      //this.sendContactFormToMail(form);
      //TODO do edycji czekamy na info od nich
      console.log(this.recoveryForm.getRawValue());
    }
  }

  /**
   * Sprawdzanie kontrolek podczas wpisywania
   */
  onControlValueChanged(): void {
    const form = this.contactForm;

    for (let field in this.formErrors) {
      this.formErrors[field] = '';
      let control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const validationMessages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key];
        }
      }
    }
  }

  /***
   * Generowanie alertu o prawidłowej wysyłce
   * @param type
   */
  generateAlert(type: string): void {
    this.alertType = type;

    if (type === 'error') {
      document.querySelector('.container').classList.add('send-error');
    } else {
      document.querySelector('.container').classList.add('send-complete');
    }

    setTimeout(() => {
      document.querySelector('.container').classList.remove(...['send-error', 'send-complete']);
    }, 3000);
  }

  /**
   * Pokazanie/ukrycie formularza resetu hasła
   */
  toogleForgotPasswordForm(): void {
    this.isRecoveryFormVisible = !this.isRecoveryFormVisible;
  }

  ngOnDestroy() {
    this._destroy$.next();
  }
}
