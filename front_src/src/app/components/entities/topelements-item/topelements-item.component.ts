import { Component, OnDestroy, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { TopElements } from '../../../classes/entities/TopElements/TopElements';
import { TopElementsService } from '../../../classes/entities/TopElements/TopElements.service';

import { Observable, Subject } from 'rxjs';
import { AnimationsService } from "../../custom/animations/animations.service";
import { takeUntil, tap } from "rxjs/operators";
import { MenuService } from "../../custom/menu/menu.service";

@Component({
  selector: 'topelements-item',
  templateUrl: './topelements-item.component.html',
  styleUrls: ['./topelements-item.component.scss'],
  providers: [TopElementsService]
})
export class TopElementsItemComponent extends ItemComponentModel<TopElements> implements OnInit, OnDestroy {
  public data$: Observable<TopElements>;
  private _destroy$: Subject<boolean> = new Subject<boolean>();

  @Output() dataLoaded: EventEmitter<boolean> = new EventEmitter(false);

  constructor(
    private topElementsService: TopElementsService<TopElements>,
    private animationService: AnimationsService,
    private menuService: MenuService,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(topElementsService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      tap(() => this.dataLoaded.emit(true))
    );

  }

  /**
   * Włącza video
   * @param {HTMLVideoElement} video
   */
  playVideo(video: HTMLVideoElement) {
    this.animationService.checkAOSstate()
      .pipe(
        takeUntil(this._destroy$)
      )
      .subscribe((state: boolean) => {
      if (state) {
        video.muted = true;
        video.play();
      }
    });
  }

  ngOnDestroy() {
    this._destroy$.next();
  }
}
