import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { SocialMedia } from '../../../classes/entities/SocialMedia/SocialMedia';
import { SocialMediaService } from '../../../classes/entities/SocialMedia/SocialMedia.service';

@Component({
  selector: 'socialmedia-list',
  templateUrl: './socialmedia-list.component.html',
  styleUrls: ['./socialmedia-list.component.scss'],
  providers: [SocialMediaService]
})
export class SocialMediaListComponent extends ListComponentModel<SocialMedia> implements OnInit {
  public item$: Observable<SocialMedia[]>;

  constructor(
    private socialMediaService: SocialMediaService<SocialMedia>,
    public translationService: TranslationService
  ) {
    super(socialMediaService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
