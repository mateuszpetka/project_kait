import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { ServicePackages } from '../../../classes/entities/ServicePackages/ServicePackages';
import { ServicePackagesService } from '../../../classes/entities/ServicePackages/ServicePackages.service';

import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ServicePackagesList } from '../../../shared/assemble.interface';
import { AssembleService } from "../../../shared/assemble.service";

@Component({
  selector: 'servicepackages-item',
  templateUrl: './servicepackages-item.component.html',
  styleUrls: ['./servicepackages-item.component.scss'],
  providers: [ServicePackagesService]
})
export class ServicePackagesItemComponent extends ItemComponentModel<ServicePackages> implements OnInit {
  public data$: Observable<ServicePackages>;
  public servicePackagesList$: Observable<ServicePackagesList[]>;

  constructor(
    private servicePackagesService: ServicePackagesService<ServicePackages>,
    private assembleService: AssembleService,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(servicePackagesService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );

    this.servicePackagesList$ = this.translationService.currentLangObservator.pipe(
      switchMap((lang) => this.assembleService.getServicePackageList(lang))
    );
  }
}
