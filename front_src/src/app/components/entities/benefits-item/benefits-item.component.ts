import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { Benefits } from '../../../classes/entities/Benefits/Benefits';
import { BenefitsService } from '../../../classes/entities/Benefits/Benefits.service';

import { Observable } from 'rxjs';
import { switchMap } from "rxjs/operators";
import { BenefitsList } from "../../../shared/assemble.interface";
import { AssembleService } from "../../../shared/assemble.service";

@Component({
  selector: 'benefits-item',
  templateUrl: './benefits-item.component.html',
  styleUrls: ['./benefits-item.component.scss'],
  providers: [BenefitsService]
})
export class BenefitsItemComponent extends ItemComponentModel<Benefits> implements OnInit {
  public data$: Observable<Benefits>;
  public benefitsList$: Observable<BenefitsList[]>;

  constructor(
    private benefitsService: BenefitsService<Benefits>,
    private assembleService: AssembleService,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(benefitsService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );

    this.benefitsList$ = this.translationService.currentLangObservator.pipe(
      switchMap((lang) => this.assembleService.getBenefitsList(lang))
    );
  }
}
