import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { PrivacyPolicy } from '../../../classes/entities/PrivacyPolicy/PrivacyPolicy';
import { PrivacyPolicyService } from '../../../classes/entities/PrivacyPolicy/PrivacyPolicy.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'privacypolicy-item',
  templateUrl: './privacypolicy-item.component.html',
  styleUrls: ['./privacypolicy-item.component.scss'],
  providers: [PrivacyPolicyService]
})
export class PrivacyPolicyItemComponent extends ItemComponentModel<PrivacyPolicy> implements OnInit {
  public data$: Observable<PrivacyPolicy>;

  constructor(
    private privacyPolicyService: PrivacyPolicyService<PrivacyPolicy>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(privacyPolicyService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
