import { Component } from '@angular/core';
import { TranslationService } from '../../translation.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  public activeLang: string = this.translationService.getLang();
  public availableLang$: Observable<string[]> = this.translationService.availableLanguagesBS;

  constructor(
    private translationService: TranslationService
  ) {
    this.translationService.currentLangObservator.subscribe(lang => {
      this.activeLang = lang;
    });
  }

  /**
   * Zmiana języka
   *
   * @param {string} lang
   */
  public setLang(lang: string): void {
    localStorage.setItem('lang', lang);
    this.translationService.setLang(lang);
  }
}
