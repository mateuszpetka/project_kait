import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingRecordTag } from '../../../classes/bling/BlingRecordTag/BlingRecordTag';
import { BlingRecordTagService } from '../../../classes/bling/BlingRecordTag/BlingRecordTag.service';

@Component({
  selector: 'blingrecordtag-list',
  templateUrl: './blingrecordtag-list.component.html',
  styleUrls: ['./blingrecordtag-list.component.scss'],
  providers: [BlingRecordTagService]
})
export class BlingRecordTagListComponent extends ListComponentModel<BlingRecordTag> implements OnInit {
  public item$: Observable<BlingRecordTag[]>;

  constructor(
    private blingRecordTagService: BlingRecordTagService<BlingRecordTag>,
    public translationService: TranslationService
  ) {
    super(blingRecordTagService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
