import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingMetaTag } from '../../../classes/bling/BlingMetaTag/BlingMetaTag';
import { BlingMetaTagService } from '../../../classes/bling/BlingMetaTag/BlingMetaTag.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingmetatag-item',
  templateUrl: './blingmetatag-item.component.html',
  styleUrls: ['./blingmetatag-item.component.scss'],
  providers: [BlingMetaTagService]
})
export class BlingMetaTagItemComponent extends ItemComponentModel<BlingMetaTag> implements OnInit {
  public data$: Observable<BlingMetaTag>;

  constructor(
    private blingMetaTagService: BlingMetaTagService<BlingMetaTag>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingMetaTagService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
