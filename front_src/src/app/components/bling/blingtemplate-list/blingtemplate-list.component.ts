import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingTemplate } from '../../../classes/bling/BlingTemplate/BlingTemplate';
import { BlingTemplateService } from '../../../classes/bling/BlingTemplate/BlingTemplate.service';

@Component({
  selector: 'blingtemplate-list',
  templateUrl: './blingtemplate-list.component.html',
  styleUrls: ['./blingtemplate-list.component.scss'],
  providers: [BlingTemplateService]
})
export class BlingTemplateListComponent extends ListComponentModel<BlingTemplate> implements OnInit {
  public item$: Observable<BlingTemplate[]>;

  constructor(
    private blingTemplateService: BlingTemplateService<BlingTemplate>,
    public translationService: TranslationService
  ) {
    super(blingTemplateService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
