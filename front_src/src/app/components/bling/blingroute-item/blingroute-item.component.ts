import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingRoute } from '../../../classes/bling/BlingRoute/BlingRoute';
import { BlingRouteService } from '../../../classes/bling/BlingRoute/BlingRoute.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingroute-item',
  templateUrl: './blingroute-item.component.html',
  styleUrls: ['./blingroute-item.component.scss'],
  providers: [BlingRouteService]
})
export class BlingRouteItemComponent extends ItemComponentModel<BlingRoute> implements OnInit {
  public data$: Observable<BlingRoute>;

  constructor(
    private blingRouteService: BlingRouteService<BlingRoute>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingRouteService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
