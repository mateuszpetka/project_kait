import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingRouteTranslation } from '../../../classes/bling/BlingRouteTranslation/BlingRouteTranslation';
import { BlingRouteTranslationService } from '../../../classes/bling/BlingRouteTranslation/BlingRouteTranslation.service';

@Component({
  selector: 'blingroutetranslation-list',
  templateUrl: './blingroutetranslation-list.component.html',
  styleUrls: ['./blingroutetranslation-list.component.scss'],
  providers: [BlingRouteTranslationService]
})
export class BlingRouteTranslationListComponent extends ListComponentModel<BlingRouteTranslation> implements OnInit {
  public item$: Observable<BlingRouteTranslation[]>;

  constructor(
    private blingRouteTranslationService: BlingRouteTranslationService<BlingRouteTranslation>,
    public translationService: TranslationService
  ) {
    super(blingRouteTranslationService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
