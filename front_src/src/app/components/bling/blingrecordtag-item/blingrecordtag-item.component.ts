import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingRecordTag } from '../../../classes/bling/BlingRecordTag/BlingRecordTag';
import { BlingRecordTagService } from '../../../classes/bling/BlingRecordTag/BlingRecordTag.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingrecordtag-item',
  templateUrl: './blingrecordtag-item.component.html',
  styleUrls: ['./blingrecordtag-item.component.scss'],
  providers: [BlingRecordTagService]
})
export class BlingRecordTagItemComponent extends ItemComponentModel<BlingRecordTag> implements OnInit {
  public data$: Observable<BlingRecordTag>;

  constructor(
    private blingRecordTagService: BlingRecordTagService<BlingRecordTag>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingRecordTagService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
