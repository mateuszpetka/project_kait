import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingRecordFile } from '../../../classes/bling/BlingRecordFile/BlingRecordFile';
import { BlingRecordFileService } from '../../../classes/bling/BlingRecordFile/BlingRecordFile.service';

@Component({
  selector: 'blingrecordfile-list',
  templateUrl: './blingrecordfile-list.component.html',
  styleUrls: ['./blingrecordfile-list.component.scss'],
  providers: [BlingRecordFileService]
})
export class BlingRecordFileListComponent extends ListComponentModel<BlingRecordFile> implements OnInit {
  public item$: Observable<BlingRecordFile[]>;

  constructor(
    private blingRecordFileService: BlingRecordFileService<BlingRecordFile>,
    public translationService: TranslationService
  ) {
    super(blingRecordFileService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
