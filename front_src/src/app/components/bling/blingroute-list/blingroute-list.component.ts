import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingRoute } from '../../../classes/bling/BlingRoute/BlingRoute';
import { BlingRouteService } from '../../../classes/bling/BlingRoute/BlingRoute.service';

@Component({
  selector: 'blingroute-list',
  templateUrl: './blingroute-list.component.html',
  styleUrls: ['./blingroute-list.component.scss'],
  providers: [BlingRouteService]
})
export class BlingRouteListComponent extends ListComponentModel<BlingRoute> implements OnInit {
  public item$: Observable<BlingRoute[]>;

  constructor(
    private blingRouteService: BlingRouteService<BlingRoute>,
    public translationService: TranslationService
  ) {
    super(blingRouteService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
