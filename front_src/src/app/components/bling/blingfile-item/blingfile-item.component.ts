import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingFile } from '../../../classes/bling/BlingFile/BlingFile';
import { BlingFileService } from '../../../classes/bling/BlingFile/BlingFile.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingfile-item',
  templateUrl: './blingfile-item.component.html',
  styleUrls: ['./blingfile-item.component.scss'],
  providers: [BlingFileService]
})
export class BlingFileItemComponent extends ItemComponentModel<BlingFile> implements OnInit {
  public data$: Observable<BlingFile>;

  constructor(
    private blingFileService: BlingFileService<BlingFile>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingFileService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
