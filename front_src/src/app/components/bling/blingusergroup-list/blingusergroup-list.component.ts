import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingUserGroup } from '../../../classes/bling/BlingUserGroup/BlingUserGroup';
import { BlingUserGroupService } from '../../../classes/bling/BlingUserGroup/BlingUserGroup.service';

@Component({
  selector: 'blingusergroup-list',
  templateUrl: './blingusergroup-list.component.html',
  styleUrls: ['./blingusergroup-list.component.scss'],
  providers: [BlingUserGroupService]
})
export class BlingUserGroupListComponent extends ListComponentModel<BlingUserGroup> implements OnInit {
  public item$: Observable<BlingUserGroup[]>;

  constructor(
    private blingUserGroupService: BlingUserGroupService<BlingUserGroup>,
    public translationService: TranslationService
  ) {
    super(blingUserGroupService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
