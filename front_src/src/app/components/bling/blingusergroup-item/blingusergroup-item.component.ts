import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingUserGroup } from '../../../classes/bling/BlingUserGroup/BlingUserGroup';
import { BlingUserGroupService } from '../../../classes/bling/BlingUserGroup/BlingUserGroup.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingusergroup-item',
  templateUrl: './blingusergroup-item.component.html',
  styleUrls: ['./blingusergroup-item.component.scss'],
  providers: [BlingUserGroupService]
})
export class BlingUserGroupItemComponent extends ItemComponentModel<BlingUserGroup> implements OnInit {
  public data$: Observable<BlingUserGroup>;

  constructor(
    private blingUserGroupService: BlingUserGroupService<BlingUserGroup>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingUserGroupService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
