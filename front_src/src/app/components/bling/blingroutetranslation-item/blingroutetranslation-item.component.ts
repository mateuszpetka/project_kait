import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingRouteTranslation } from '../../../classes/bling/BlingRouteTranslation/BlingRouteTranslation';
import { BlingRouteTranslationService } from '../../../classes/bling/BlingRouteTranslation/BlingRouteTranslation.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingroutetranslation-item',
  templateUrl: './blingroutetranslation-item.component.html',
  styleUrls: ['./blingroutetranslation-item.component.scss'],
  providers: [BlingRouteTranslationService]
})
export class BlingRouteTranslationItemComponent extends ItemComponentModel<BlingRouteTranslation> implements OnInit {
  public data$: Observable<BlingRouteTranslation>;

  constructor(
    private blingRouteTranslationService: BlingRouteTranslationService<BlingRouteTranslation>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingRouteTranslationService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
