import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingRecordFile } from '../../../classes/bling/BlingRecordFile/BlingRecordFile';
import { BlingRecordFileService } from '../../../classes/bling/BlingRecordFile/BlingRecordFile.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingrecordfile-item',
  templateUrl: './blingrecordfile-item.component.html',
  styleUrls: ['./blingrecordfile-item.component.scss'],
  providers: [BlingRecordFileService]
})
export class BlingRecordFileItemComponent extends ItemComponentModel<BlingRecordFile> implements OnInit {
  public data$: Observable<BlingRecordFile>;

  constructor(
    private blingRecordFileService: BlingRecordFileService<BlingRecordFile>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingRecordFileService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
