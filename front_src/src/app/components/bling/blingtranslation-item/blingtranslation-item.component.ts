import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingTranslation } from '../../../classes/bling/BlingTranslation/BlingTranslation';
import { BlingTranslationService } from '../../../classes/bling/BlingTranslation/BlingTranslation.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingtranslation-item',
  templateUrl: './blingtranslation-item.component.html',
  styleUrls: ['./blingtranslation-item.component.scss'],
  providers: [BlingTranslationService]
})
export class BlingTranslationItemComponent extends ItemComponentModel<BlingTranslation> implements OnInit {
  public data$: Observable<BlingTranslation>;

  constructor(
    private blingTranslationService: BlingTranslationService<BlingTranslation>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingTranslationService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
