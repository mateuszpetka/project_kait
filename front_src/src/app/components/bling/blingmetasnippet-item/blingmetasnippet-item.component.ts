import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingMetaSnippet } from '../../../classes/bling/BlingMetaSnippet/BlingMetaSnippet';
import { BlingMetaSnippetService } from '../../../classes/bling/BlingMetaSnippet/BlingMetaSnippet.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingmetasnippet-item',
  templateUrl: './blingmetasnippet-item.component.html',
  styleUrls: ['./blingmetasnippet-item.component.scss'],
  providers: [BlingMetaSnippetService]
})
export class BlingMetaSnippetItemComponent extends ItemComponentModel<BlingMetaSnippet> implements OnInit {
  public data$: Observable<BlingMetaSnippet>;

  constructor(
    private blingMetaSnippetService: BlingMetaSnippetService<BlingMetaSnippet>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingMetaSnippetService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
