import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingMetaTag } from '../../../classes/bling/BlingMetaTag/BlingMetaTag';
import { BlingMetaTagService } from '../../../classes/bling/BlingMetaTag/BlingMetaTag.service';

@Component({
  selector: 'blingmetatag-list',
  templateUrl: './blingmetatag-list.component.html',
  styleUrls: ['./blingmetatag-list.component.scss'],
  providers: [BlingMetaTagService]
})
export class BlingMetaTagListComponent extends ListComponentModel<BlingMetaTag> implements OnInit {
  public item$: Observable<BlingMetaTag[]>;

  constructor(
    private blingMetaTagService: BlingMetaTagService<BlingMetaTag>,
    public translationService: TranslationService
  ) {
    super(blingMetaTagService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
