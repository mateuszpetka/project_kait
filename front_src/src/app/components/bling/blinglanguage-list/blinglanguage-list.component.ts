import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingLanguage } from '../../../classes/bling/BlingLanguage/BlingLanguage';
import { BlingLanguageService } from '../../../classes/bling/BlingLanguage/BlingLanguage.service';

@Component({
  selector: 'blinglanguage-list',
  templateUrl: './blinglanguage-list.component.html',
  styleUrls: ['./blinglanguage-list.component.scss'],
  providers: [BlingLanguageService]
})
export class BlingLanguageListComponent extends ListComponentModel<BlingLanguage> implements OnInit {
  public item$: Observable<BlingLanguage[]>;

  constructor(
    private blingLanguageService: BlingLanguageService<BlingLanguage>,
    public translationService: TranslationService
  ) {
    super(blingLanguageService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
