import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingFile } from '../../../classes/bling/BlingFile/BlingFile';
import { BlingFileService } from '../../../classes/bling/BlingFile/BlingFile.service';

@Component({
  selector: 'blingfile-list',
  templateUrl: './blingfile-list.component.html',
  styleUrls: ['./blingfile-list.component.scss'],
  providers: [BlingFileService]
})
export class BlingFileListComponent extends ListComponentModel<BlingFile> implements OnInit {
  public item$: Observable<BlingFile[]>;

  constructor(
    private blingFileService: BlingFileService<BlingFile>,
    public translationService: TranslationService
  ) {
    super(blingFileService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
