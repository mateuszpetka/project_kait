import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingMetaSnippet } from '../../../classes/bling/BlingMetaSnippet/BlingMetaSnippet';
import { BlingMetaSnippetService } from '../../../classes/bling/BlingMetaSnippet/BlingMetaSnippet.service';

@Component({
  selector: 'blingmetasnippet-list',
  templateUrl: './blingmetasnippet-list.component.html',
  styleUrls: ['./blingmetasnippet-list.component.scss'],
  providers: [BlingMetaSnippetService]
})
export class BlingMetaSnippetListComponent extends ListComponentModel<BlingMetaSnippet> implements OnInit {
  public item$: Observable<BlingMetaSnippet[]>;

  constructor(
    private blingMetaSnippetService: BlingMetaSnippetService<BlingMetaSnippet>,
    public translationService: TranslationService
  ) {
    super(blingMetaSnippetService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
