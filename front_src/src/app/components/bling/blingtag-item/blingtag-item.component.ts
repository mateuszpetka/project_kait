import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from '../../../translation.service';
import { ItemComponentModel } from '../../../tools/models/item-component.model';

import { BlingTag } from '../../../classes/bling/BlingTag/BlingTag';
import { BlingTagService } from '../../../classes/bling/BlingTag/BlingTag.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'blingtag-item',
  templateUrl: './blingtag-item.component.html',
  styleUrls: ['./blingtag-item.component.scss'],
  providers: [BlingTagService]
})
export class BlingTagItemComponent extends ItemComponentModel<BlingTag> implements OnInit {
  public data$: Observable<BlingTag>;

  constructor(
    private blingTagService: BlingTagService<BlingTag>,
    public translationService: TranslationService,
    public router: Router
  ) {
    super(blingTagService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.data$ = this.modelData$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
