import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../translation.service';
import { ListComponentModel } from '../../../tools/models/list-component.model';

import { Observable } from 'rxjs';

import { BlingTag } from '../../../classes/bling/BlingTag/BlingTag';
import { BlingTagService } from '../../../classes/bling/BlingTag/BlingTag.service';

@Component({
  selector: 'blingtag-list',
  templateUrl: './blingtag-list.component.html',
  styleUrls: ['./blingtag-list.component.scss'],
  providers: [BlingTagService]
})
export class BlingTagListComponent extends ListComponentModel<BlingTag> implements OnInit {
  public item$: Observable<BlingTag[]>;

  constructor(
    private blingTagService: BlingTagService<BlingTag>,
    public translationService: TranslationService
  ) {
    super(blingTagService, translationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.item$ = this.modelItem$.pipe(
      /**
       * Tutaj możesz zrobić operacje na liście rekordów zwracanych przez service
       *
       * np.:
       * tap( (values) => this.ileTuJestElementow = values.length)
       *
       * Ważne: Operatory powinny być importowane z 'rxjs/operators'!
       */
    );
  }
}
