import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { QueryParams } from '../../../classes/QueryParams';
import { FooterMenuList, Copyright } from '../../../shared/assemble.interface';
import { TranslationService } from "../../../translation.service";
import { AssembleService } from "../../../shared/assemble.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public footerMenuList$: Observable<FooterMenuList[]>;
  public footerCopyright$: Observable<Copyright[]>;

  constructor(
    private assembleService: AssembleService,
    private translationService: TranslationService
  ) { }

  ngOnInit() {
    this.translationService.currentLangObservator.pipe(
      tap((lang) => {
        this.footerMenuList$ = this.assembleService.getFooterMenuList(lang);
        this.footerCopyright$ = this.assembleService.getCopyrightContent(lang);
      })
    ).subscribe();
  }
}
