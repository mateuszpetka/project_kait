import { ApplicationInitStatus, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { delay } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AnimationsService {
  private _animation: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private _initAnimation: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public initAnimation$: Observable<boolean> = this._initAnimation.asObservable().pipe(delay(300));

  private _routeAnimation$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

  /**
   * Ustawia status animacji
   * @param {boolean} state
   */
  setAOSstate(state: boolean): void {
    this._animation.next(state);
  }

  /**
   *  Sprawdza status animacji
   * @return {Observable<boolean>}
   */
  checkAOSstate(): Observable<boolean> {
    return this._animation;
  }

  /**
   * Zwraca status inicjalizacji apki
   * @return {Observable<boolean>}
   */
  checkInitState(): Observable<boolean> {
    return this._initAnimation;
  }

  /**
   * Zmienia status inicjalizacji apki
   * @param state
   */
  changeInitState(state): void {
    this._initAnimation.next(state);
  }

  /**
   * Zwraca status animacji routingu
   * @return {Observable<boolean>}
   */
  checkRouteAnimationState(): Observable<boolean> {
    return this._routeAnimation$;
  }

  /**
   * Zmienia status animacji routingu
   * @param state
   */
  changeRouteAnimationState(state): void {
    this._routeAnimation$.next(state);
  }

}
