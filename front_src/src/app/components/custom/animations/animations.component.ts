import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  ViewChild,
  Input
} from '@angular/core';
import { AssembleService } from "../../../shared/assemble.service";
import { TranslationService } from "../../../translation.service";
import { takeUntil, tap, shareReplay } from "rxjs/operators";
import { Observable, Subject, combineLatest } from "rxjs";
import { TopMenuLogo } from "../../../shared/assemble.interface";
import { AnimationsService } from "./animations.service";
import anime from 'animejs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-animations',
  templateUrl: './animations.component.html',
  styleUrls: ['./animations.component.scss']
})
export class AnimationsComponent implements AfterViewInit, OnDestroy {
  public topMenuLogo$: Observable<TopMenuLogo>;
  public initAnimation$: Observable<boolean>;
  @Input() initAnimation: boolean = true;

  @ViewChild('animeContainer') animeContainer: ElementRef<HTMLElement>;
  @ViewChild('titleTop') titleTop: ElementRef<HTMLElement>;
  @ViewChild('titleBottom') titleBottom: ElementRef<HTMLElement>;
  @ViewChild('logo') logo: ElementRef<HTMLElement>;
  @ViewChild('line') line: ElementRef<HTMLElement>;
  @ViewChild('overflowRectangle') overflowRectangle: ElementRef<HTMLElement>;
  @ViewChild('overflowContainer') overflowContainer: ElementRef<HTMLElement>;
  @ViewChild('overflowContainerFirst') overflowContainerFirst: ElementRef<HTMLElement>;
  @ViewChild('overflowContainerSecond') overflowContainerSecond: ElementRef<HTMLElement>;
  @ViewChild('overflowContainerThird') overflowContainerThird: ElementRef<HTMLElement>;
  
  
  //Route elements
  @ViewChild('routeContainer') routeContainer: ElementRef<HTMLElement>;
  @ViewChild('routeContainerFirst') routeContainerFirst: ElementRef<HTMLElement>;
  @ViewChild('routeContainerSecond') routeContainerSecond: ElementRef<HTMLElement>;
  @ViewChild('routeContainerThird') routeContainerThird: ElementRef<HTMLElement>;

  private _destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private assembleService: AssembleService,
    private translationService: TranslationService,
    private animationService: AnimationsService,
    private _router: Router
  ) { }


  ngAfterViewInit(): void {
    this.startInitAnimation();

    this.translationService.currentLangObservator.pipe(
      takeUntil(this._destroy$),
      tap((lang) => {
        this.topMenuLogo$ = this.assembleService.getTopMenuLogo(lang);
      })
    ).subscribe();

    this.animationService.checkRouteAnimationState()
      .subscribe((isAnimating: boolean) => {
        if (isAnimating) {
          this.routeAnimation();
        }
    });
  }

  startInitAnimation() {
    let line = anime.timeline({
      targets: this.line.nativeElement,
      easing: 'linear',
      complete: () => {
        this.animationService.setAOSstate(true);
      }
    });

    line
      .add({
        delay: 500,
        duration: 500,
        width: ['1px', '100%'],
        right: ['100%', 0]
      })
      .add({
        targets: this.titleTop.nativeElement,
        easing: 'cubicBezier(.56,-0.18,.66,1.1)',
        delay: 200,
        duration: 200,
        translateX: ['-10px', '-7px'],
        translateY: ['100%', '0%']
      })
      .add({
        targets: this.titleBottom.nativeElement,
        easing: 'cubicBezier(.56,-0.18,.66,1.1)',
        delay: 200,
        duration: 200,
        translateX: ['10px', '7px'],
        translateY: ['-100%', '0%']
      })
      .add({
        targets: this.titleTop.nativeElement,
        easing: 'cubicBezier(.56,-0.18,.66,1.1)',
        duration: 150,
        translateX: ['-7px', '0px']
      })
      .add({
        targets: this.titleBottom.nativeElement,
        easing: 'cubicBezier(.56,-0.18,.66,1.1)',
        duration: 150,
        translateX: ['7px', '0px']
      })
      .add({
        delay: 100,
        duration: 200,
        easing: 'easeInOutSine',
        width: '50px',
        right: '-50px',
      })
      .add({
        delay: 50,
        duration: 100,
        easing: 'easeInOutSine',
        rotate: '-10deg'
      })
      .add({
        easing: 'easeInOutSine',
        delay: 50,
        duration: 100,
        rotate: '90deg'
      })
      .add({
        easing: 'easeInOutSine',
        delay: 50,
        duration: 100,
        rotate: '270deg'
      })
      .add({
        duration: 200,
        width: '150px',
        right: '-100px'
      })
      .add({
        delay: 200,
        duration: 100,
        width: '150px',
        right: '-104px'
      })
      .add({
        duration: 200,
        right: '100%',
        update: (anim) => {
          const progress: number = Math.round(anim.progress);

          if (progress > 40) {
            anime({
              targets: this.overflowRectangle.nativeElement,
              duration: 50,
              right: '0'
            })
          }
        }
      })
      .add({
        targets: this.logo.nativeElement,
        easing: 'easeInOutQuad',
        duration: 200,
        opacity: 1
      })
      .add({
        easing: 'easeInOutQuad',
        duration: 200,
        scale: 1.1
      })
      .add({
        easing: 'easeInOutQuad',
        delay: 200,
        duration: 150,
        scale: 1,
        width: '50px',
        right: 'calc(100% + 48px)'
      })
      .add({
        easing: 'easeInOutQuad',
        duration: 100,
        width: '0px',
        right: 'calc(100% + 72px)'
      })
      .add({
        targets: this.overflowContainer.nativeElement,
        easing: 'easeInOutSine',
        duration: 1000,
        loopBegin: (anim) => {
          this.overflowContainer.nativeElement.classList.add('animate');
        },
        complete: (anim) => {
          this.logo.nativeElement.classList.add('logo-hidden');

          anime({
            targets: this.animeContainer.nativeElement,
            duration: 800,
            opacity: 0
          });
        }
      });
  }

  routeAnimation() {
    this.routeContainer.nativeElement.style.opacity = '1';
    this.routeContainer.nativeElement.style.zIndex = '99';
    this.routeContainer.nativeElement.classList.remove('animate');

    let routeContainer = anime.timeline({
      targets: this.routeContainer.nativeElement,
      easing: 'linear',
      complete: () => {
        this.animationService.setAOSstate(true);
      }
    });

    routeContainer
      .add({
        targets: this.routeContainer.nativeElement,
        easing: 'easeInOutSine',
        duration: 1000,
        loopBegin: (anim) => {
          this.routeContainer.nativeElement.classList.add('animate');
        },
        complete: (anim) => {
          anime({
            targets: this.routeContainer.nativeElement,
            duration: 800,
            opacity: 0
          })
        }
      });
  }

  ngOnDestroy() {
    this._destroy$.next();
  }
}
