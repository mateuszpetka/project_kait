import { Component, Input, OnInit } from '@angular/core';
import { MenuService } from "../menu/menu.service";

@Component({
  selector: 'app-redirect-button',
  templateUrl: './redirect-button.component.html',
  styleUrls: ['./redirect-button.component.scss']
})
export class RedirectButtonComponent implements OnInit {
  @Input('url') url: string = '';
  @Input('name') name: string = '';
  @Input('position') position: string = '';
  @Input('type') type: string = '';

  constructor(
    private menuService: MenuService
  ) { }

  ngOnInit() {
  }

  /**
   * Wlacza routing i animacje przejscia
   * @param {Event} e
   */
  route(e: Event): void {
    this.menuService.route(e);
  }

}
