import {
  Component,
  OnInit,
  OnDestroy,
  Renderer2,
  Input,
  ViewChildren,
  ElementRef,
  ViewChild,
  QueryList
} from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { filter, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { QueryParams } from '../../../classes/QueryParams';
import { TopMenuLogo, TopMenuList } from '../../../shared/assemble.interface';
import { TranslationService } from "../../../translation.service";
import { AssembleService } from "../../../shared/assemble.service";
import { ActivatedRoute, ActivatedRouteSnapshot, ActivationEnd, Router, NavigationEnd } from "@angular/router";
import { Location } from "@angular/common";
import { AnimationsService } from "../animations/animations.service";
import AOS from 'aos'
import { MenuService } from "./menu.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public topMenuLogo$: Observable<TopMenuLogo>;
  public topMenuLists$: Observable<TopMenuList[]>;
  public activeUrl: string;
  public toggleMenu: boolean = false;
  public firstLoad: boolean = true;
  public animatingRoute: Observable<boolean>;

  private _destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private assembleService: AssembleService,
    private translationService: TranslationService,
    private renderer: Renderer2,
    private animationService: AnimationsService,
    private router: Router,
    private menuService: MenuService
  ) { }

  ngOnInit() {
    this.translationService.currentLangObservator.pipe(
      tap((lang) => {
        this.topMenuLogo$ = this.assembleService.getTopMenuLogo(lang);
        this.topMenuLists$ = this.assembleService.getTopMenuList(lang);
      })
    ).subscribe();

    this.router.events.pipe(
      filter(event => event instanceof ActivationEnd)
    ).subscribe(event => {
      const slug = (event as ActivatedRoute).snapshot.url[0].path;
      this.activeUrl = slug;
      this.renderer.setStyle(document.body, 'overflow-y', 'scroll');
      this.renderer.setStyle(document.body, 'overflow-x', 'hidden');
      this.firstLoad = false;
      window.scrollTo(0, 0);
    });

    this.menuService.toggleMenuState.pipe(
      takeUntil(this._destroy$),
      tap((state: boolean) => {
        this.toggleMenu = state;
      })
    ).subscribe();
  }

  /**
   * Pokazuje / Ukrywa menu na mobile
   */
  toggleMobileMenu(): void {
    this.toggleMenu = !this.toggleMenu;

    !!this.toggleMenu ?
      this.renderer.setStyle(document.body, 'overflow', 'hidden') :
      this.renderer.setStyle(document.body, 'overflow', '');
  }

  /**
   * Routing + wlaczenie animacji routingu
   * @param {Event} event
   */
  route(event: Event): void {
    this.toggleMenu = false;
    this.menuService.route(event);
  }

  ngOnDestroy() {
    this._destroy$.next();
  }
}
