import { Injectable } from '@angular/core';
import { AnimationsService } from "../animations/animations.service";
import { Router } from "@angular/router";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  public toggleMenuState: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private animationService: AnimationsService,
    private router: Router
  ) { }

  /**
   * Event na click w menu
   * @param {Event} event
   */
  route(event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    const domainRegex = /https?:\/\/.*?\//;
    const parent = (event.target as HTMLElement).parentElement;
    const parsedUrl = (parent as HTMLLinkElement).href.replace(domainRegex, '');
    parent.blur();

    if ((parent as HTMLLinkElement).href !== location.href) {
      this.animationService.changeRouteAnimationState(true);
      setTimeout(() => {
        this.router.navigateByUrl(parsedUrl);
        this.animationService.changeRouteAnimationState(false);
        this.toggleMenuState.next(false);
      }, 550);
    } else {
      document.body.scrollIntoView({behavior: 'smooth', block: 'start'});
    }
  }
}
