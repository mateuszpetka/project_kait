import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription, Subject } from "rxjs";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { TranslationService } from "../../../translation.service";
import { HttpService } from "../../../http.service";
import { debounceTime, switchMap, takeUntil, tap } from "rxjs/operators";
import { AssembleService } from "../../../shared/assemble.service";
import { ContactFormReplyTo, ContactFormSettings } from "../../../shared/assemble.interface";
import { FormFields, FormSendData } from "../../../shared/interfaces.interface";
import { SharedService } from "../../../shared/shared.service";

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit, OnDestroy {
  public lang: string;
  public contactForm: FormGroup;
  public alertType: string = '';
  public formErrors: FormFields = new FormFields();

  private contactFormReplyTo$: Observable<ContactFormReplyTo[]>;
  public contactFormSettings$: Observable<ContactFormSettings>;
  private contactFormSendData: FormSendData;
  private langSubscribe: Subscription;
  private validationMessages = {
    contactFormName: {
      required: 'form.errors.firstname.required',
      minlength: 'form.errors.firstname.minlength'
    },
    contactFormEmail: {
      email: 'form.errors.email.validmail',
      required: 'form.errors.email.required'
    },
    contactFormMessage: {
      required: 'form.errors.msg.required'
    }
  };
  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private translationService: TranslationService,
    private http: HttpService,
    private formBuilder: FormBuilder,
    private assembleService: AssembleService
  ) {}

  ngOnInit() {

    this.getAssemble();
    this.assignContactFormSettings();

    /**
     * Podstawowe validatory
     * https://angular.io/api/forms/Validators
     */
    this.contactForm = this.formBuilder.group({
      contactFormName: ['', [
        Validators.required,
        Validators.minLength(3)
        ]
      ],
      contactFormEmail: ['', [
        Validators.email,
        Validators.required
        ]
      ],
      contactFormMessage: ['', [
        Validators.required
        ]
      ]
    });

    /***
     * Obserwowanie czy wartość w kontrolkach się zmieniła i walidowanie
     */
    this.contactForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(500)
    ).subscribe( (value: FormFields) => {
      this.onControlValueChanged();
    });

    this.onControlValueChanged();
  }

  /**
   * Pobieranie i przypisywanie assembli w zależności od języka
   */
  getAssemble(): void {
    this.langSubscribe = this.translationService.currentLangObservator.pipe(
      takeUntil(this.destroy$),
    ).subscribe((lang) => {
      this.lang = lang;
      this.contactFormReplyTo$ = this.assembleService.getContactFormSettingsReplyTo(lang);
      this.contactFormSettings$ = this.assembleService.getContactFormSettings(lang);
    });
  }

  /**
   * Przypisywanie wymaganych pól do wysyłki formularza
   */
  assignContactFormSettings(): void {
    this.contactFormSettings$.pipe(
      takeUntil(this.destroy$),
    ).subscribe((res: ContactFormSettings) => {
      this.contactFormSendData = {
        recipient: res.contactFormSettingsRecipient,
        subject: res.contactFormSettingsSubject,
        senderName: res.contactFormSettingsSenderName,
        senderAddress: res.contactFormSettingsSenderAddress
      }
    });
  }

  /**
   * Akcja na wysyłkę formularza kontaktowego
   * @param form
   */
  onSubmit(form: FormGroup): void {
    if (this.contactForm.valid) {
      this.sendContactFormToMail(form);
      this.http.post('ContactFormList', { ...this.contactForm.getRawValue(), status: 1 })
        .subscribe(res => {
          if (res.status) {
            this.contactForm.reset();
            this.generateAlert('success');
          } else {
            this.generateAlert('error');
          }
        });
    } else {
      this.formSubmitValidation();
    }
  };

  /**
   * Wysyłka formularza kontaktowego na maila
   * @param form
   */
  sendContactFormToMail(form: FormGroup): void {
    const body = new FormData();

    body.append('recipientEmail', `${this.contactFormSendData.recipient}`);
    body.append('subject', `${this.contactFormSendData.subject}`);
    body.append('senderName', `${this.contactFormSendData.senderName}`);
    body.append('senderAddress', `${this.contactFormSendData.senderAddress}`);
    body.append('htmlContent', `
    <ul style="list-style-type: none; padding: 0">
      <li><strong>Firstname:</strong> ${form.value.contactFormName}</li>
      <li><strong>E-mail:</strong> ${form.value.contactFormEmail}</li>
      <li><strong>Message:</strong> ${form.value.contactFormMessage}</li>
    </ul>
  `);

    this.contactFormReplyTo$.forEach(mail => {
      const replyToMail: string = mail['contactFormReplyToListEmail'];
      const replyToName: string = mail['contactFormReplyToListName'];
      body.append(`replyTo[${replyToMail}]`, `${replyToName}`);
    });

    this.http.post('Contact/mailToRecipient', body).subscribe(
      res => { },
      (error) => {
        console.error('error', error.error.msg);
      },
      () => {}
    );
  }

  /**
   * Sprawdzanie kontrolek podczas wpisywania
   */
  onControlValueChanged() {
    const form = this.contactForm;

    for (let field in this.formErrors) {
      this.formErrors[field] = '';
      let control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const validationMessages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key];
        }
      }
    }
  }

  /**
   * Sprawdzanie kontrolek podczas wysyłki
   */
  formSubmitValidation(): void {
    const form = this.contactForm;

    for (let field in this.formErrors) {
      this.formErrors[field] = '';
      let control = form.get(field);

      if (control && !control.valid) {
        const validationMessages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key];
        }
      }
    }
  }

  /***
   * Generowanie alertu o prawidłowej wysyłce
   * @param type
   */
  generateAlert(type: string): void {
    document.querySelector('.form').classList.add('send-state');
    this.alertType = type;
  }

  ngOnDestroy() {
    this.destroy$.next();
  }
}
