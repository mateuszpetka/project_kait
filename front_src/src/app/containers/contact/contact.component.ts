import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslationService } from "../../translation.service";
import { AppService } from "../../app.service";
import { HttpService } from "../../http.service";
import { Subject } from "rxjs";
import { switchMap, takeUntil, shareReplay } from "rxjs/operators";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, OnDestroy {
  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private translationService: TranslationService,
    private appService: AppService,
    private httpService: HttpService
  ){}

  ngOnInit(): void {

    // Ustawienie SEO
    this.translationService.currentLangObservator.pipe(
      takeUntil(this.destroy$),
      shareReplay(1),
      switchMap(val => {
        return this.httpService.get( val + '/Copyright/1');
      }),
    ).subscribe(val => this.appService.updateSEO(val.seoTitle, val.seoDescription, val.seoImage))
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
