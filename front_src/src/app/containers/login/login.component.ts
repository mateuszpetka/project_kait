import { Component } from '@angular/core';
import { TranslationService } from "../../translation.service";
import { AppService } from "../../app.service";
import { HttpService } from "../../http.service";
import { Subject } from "rxjs";
import { shareReplay, switchMap, takeUntil } from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private translationService: TranslationService,
    private appService: AppService,
    private httpService: HttpService
  ){}

  ngOnInit(): void {

    // Ustawienie SEO
    this.translationService.currentLangObservator.pipe(
      takeUntil(this.destroy$),
      shareReplay(1),
      switchMap(val => {
        return this.httpService.get( val + '/LoginSection/1');
      }),
    ).subscribe(val => this.appService.updateSEO(val.seoTitle, val.seoDescription, val.seoImage))
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
