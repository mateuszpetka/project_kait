import { Component, OnDestroy, OnInit } from '@angular/core';
import { AnimationsService } from "../../components/custom/animations/animations.service";
import { TranslationService } from "../../translation.service";
import { AppService } from "../../app.service";
import { HttpService } from "../../http.service";
import { Subject } from "rxjs";
import { shareReplay, switchMap, takeUntil, tap } from "rxjs/operators";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnDestroy, OnInit {
  public finishAnimations: boolean = false;
  public sectionsShown: boolean = false;

  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private animationService: AnimationsService,
    private translationService: TranslationService,
    private appService: AppService,
    private httpService: HttpService
  ) {
    this.animationService.checkAOSstate().pipe(
      takeUntil(this.destroy$),
      tap((state: boolean) => {
        this.finishAnimations = state;
      })
    ).subscribe();
  }

  ngOnInit(): void {

    // Ustawienie SEO
    this.translationService.currentLangObservator.pipe(
      takeUntil(this.destroy$),
      shareReplay(1),
      switchMap(val => {
        return this.httpService.get( val + '/TopElements/1');
      }),
    ).subscribe(val => this.appService.updateSEO(val.seoTitle, val.seoDescription, val.seoImage))
  }

  /**
   * Shows sections that are initially hidden as to avoid
   * content jumping
   *
   * @returns void
   */
  showSections(): void {
    this.sectionsShown = true;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
